#include "StaticProceduralGeometry.h"
#include "AssetRegistryModule.h"
#include "UnrealEd.h"
#include "ProceduralGeometryComponent.h"
#include "GameFramework/Actor.h"

UStaticProceduralGeometry::UStaticProceduralGeometry(const FObjectInitializer & ObjectInitializer)
	: Super(ObjectInitializer)
{

}

UStaticProceduralGeometry *UStaticProceduralGeometry::BuildAsset(UStaticMesh * pSourceMesh, const bool GeometrizeUVCuts)
{
	if (!IsValid(pSourceMesh))
		return nullptr;

	const FString AssetName = pSourceMesh->GetName() + TEXT("_procedural");
	const FString AssetPath = pSourceMesh->GetPathName().Replace(*FPackageName::GetLongPackageAssetName(pSourceMesh->GetPathName()), *AssetName);
	const FString AssetFullPath = FPaths::ConvertRelativePathToFull(FPaths::ProjectContentDir()) + AssetPath.Replace(TEXT("/Game/"), TEXT(""), ESearchCase::CaseSensitive) + FPackageName::GetAssetPackageExtension();
	const FString SkeletonPath = AssetFullPath.Replace(*(pSourceMesh->GetName() + TEXT("_procedural") + FPackageName::GetAssetPackageExtension()), *(pSourceMesh->GetName() + TEXT("_skel.fp")));
		
	UPackage *Package = CreatePackage(nullptr, *AssetPath);
	check(Package);

	// STATIC MESH BREAKDOWN
	FString SkeletonDataString;
	FFileHelper::LoadFileToString(SkeletonDataString, *SkeletonPath);
	FProceduralGeometrySkeleton SkeletonData = BuildTreeSkeletonData(SkeletonDataString);

	/* Actual asset pointer. */
	UStaticProceduralGeometry *StaticProceduralGeometry = NewObject<UStaticProceduralGeometry>(Package, UStaticProceduralGeometry::StaticClass(), *AssetName, RF_Public | RF_Standalone);

	/* Building procedural geometry from static mesh. */
	//FORCEINLINE void DrawDebugSphere(const UWorld* InWorld, FVector const& Center, float Radius, int32 Segments, FColor const& Color, bool bPersistentLines = false, float LifeTime = -1.f, uint8 DepthPriority = 0, float Thickness = 0.f) {}
	StaticProceduralGeometry->Geometries = FProceduralGeometry::CreateProceduralGeometriesFromStaticMesh(pSourceMesh, true, FColor(255, 0, 0), GeometrizeUVCuts);

	if (SkeletonData.IsSkeletonValid())
	{
		for (int i = 0; i < StaticProceduralGeometry->Geometries.Num(); i++)
		{
			FProceduralGeometry Geom = StaticProceduralGeometry->Geometries[i];

			/* Set ID for geometry. */
			Geom.SetGeometryID(i);

			if (Geom.VertexBuffer.Num() < 5)
				continue;

			FProceduralGeometrySkeleton GeomSkel;
			/* Building skeleton. */
			FActorSpawnParameters params;
			params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			AActor *pTempActor = GWorld->GetWorld()->SpawnActor<AActor>(AActor::StaticClass(), FVector(0.f, 0.f, -5000.f), FRotator(0.f, 0.f, 0.f), params);
			UProceduralGeometryComponent *pTempComponent = NewObject<UProceduralGeometryComponent>(pTempActor);
			pTempComponent->RegisterComponent();
			pTempActor->SetRootComponent(pTempComponent);
			pTempActor->SetActorLocation(FVector(0.f, 0.f, 1000.f));
			pTempComponent->Add(Geom, false);
			pTempComponent->Update();

			const FVector RayHitStart = FVector(5000.f, 0.f, 1000.f);
			const FVector G = pTempActor->GetActorLocation();

			/* Iterate through all edges inside the skeleton. */
			for (const FEdgeIndex & Edge : SkeletonData.EdgeBuffer)
			{
				/* Store start and end point of the vertex. */
				const FProceduralGeometrySkeletonVertex V0 = SkeletonData.VertexBuffer[Edge.V0];
				const FVector V0_Global = V0.Position + G;
				const FProceduralGeometrySkeletonVertex V1 = SkeletonData.VertexBuffer[Edge.V1];
				const FVector V1_Global = V1.Position + G;

				/* Check if first vertex is inside the mesh. */
				FHitResult HitResult;
				FCollisionQueryParams Params;
				Params.bTraceComplex = true;

				FEdgeIndex OutEdge;

				/* If edge point V0 is inside the mesh. */
				if (pTempComponent->LineTraceComponent(HitResult, RayHitStart, V0_Global, Params))
				{
					/* V0 happens to be one side of the skeleton. */
					/* In case this vector already exists in the buffer, just grab its index. */
					int32 vertIdx = GeomSkel.FindVertex(V0);
					if (vertIdx == INDEX_NONE)
						OutEdge.V0 = GeomSkel.VertexBuffer.Add(V0);
					else
						OutEdge.V0 = vertIdx;

					/* If edge point V1 is not inside the mesh, we will trace line from V1 to V0. */
					if (!pTempComponent->LineTraceComponent(HitResult, RayHitStart, V1_Global, Params))
					{
						/* If V1 to V0 trace hits mesh, intersection point is the real edge. */
						if (pTempComponent->LineTraceComponent(HitResult, V1_Global, V0_Global, Params))
						{
							/* Intersection point is the output edge V1 */
							vertIdx = GeomSkel.FindVertex(HitResult.Location - G);
							if (vertIdx == INDEX_NONE)
							{
								// Calculate interpolation alpha between vert 0 and 1 that is hit result.
								const float VLength = FVector::Dist(V0.Position, V1.Position);
								const float InterpLength = FVector::Dist(V0.Position, HitResult.Location - G);
								const float InterpAlpha = (InterpLength * 1.f) / VLength;
								const FVector2D InterpRadius = FVector2D(FMath::Lerp<float>(V0.Radius.X, V1.Radius.X, InterpAlpha), FMath::Lerp<float>(V0.Radius.Y, V1.Radius.Y, InterpAlpha));

								FProceduralGeometrySkeletonVertex InterpVert;
								InterpVert.Position = HitResult.Location - G;
								InterpVert.Radius = InterpRadius;
								OutEdge.V1 = GeomSkel.VertexBuffer.Add(InterpVert);
							}

							else
								OutEdge.V1 = vertIdx;
						}
					}
					/* If edge point V1 is inside mesh too, add it. */
					else
					{
						vertIdx = GeomSkel.FindVertex(V1);
						if (vertIdx == INDEX_NONE)
							OutEdge.V1 = GeomSkel.VertexBuffer.Add(V1);
						else
							OutEdge.V1 = vertIdx;
					}
				}

				/* Opposite situation, if point V0 is not inside the mesh, but point V1 is. */
				else if (pTempComponent->LineTraceComponent(HitResult, RayHitStart, V1_Global, Params))
				{
					/* V1 happens to be one side of skeleton. */
					int32 vertIdx = GeomSkel.FindVertex(V1);
					if (vertIdx == INDEX_NONE)
						OutEdge.V1 = GeomSkel.VertexBuffer.Add(V1);
					else
						OutEdge.V1 = vertIdx;

					/* If edge point V0 is not inside the mesh, we will trace line from V0 to V1. */
					if (!pTempComponent->LineTraceComponent(HitResult, RayHitStart, V0_Global, Params))
					{
						/* If V0 to V1 trace hits mesh, intersection point is the real edge. */
						if (pTempComponent->LineTraceComponent(HitResult, V0_Global, V1_Global, Params))
						{
							/* Intersection point is the output edge V0 */
							vertIdx = GeomSkel.FindVertex(HitResult.Location - G);
							if (vertIdx == INDEX_NONE)
							{
								const float VLength = FVector::Dist(V1.Position, V0.Position);
								const float InterpLength = FVector::Dist(V1.Position, HitResult.Location - G);
								const float InterpAlpha = (InterpLength * 1.f) / VLength;
								const FVector2D InterpRadius = FVector2D(FMath::Lerp<float>(V1.Radius.X, V0.Radius.X, InterpAlpha), FMath::Lerp<float>(V1.Radius.Y, V0.Radius.Y, InterpAlpha));

								FProceduralGeometrySkeletonVertex InterpVert;
								InterpVert.Position = HitResult.Location - G;
								InterpVert.Radius = InterpRadius;
								OutEdge.V0 = GeomSkel.VertexBuffer.Add(InterpVert);
							}

							else
								OutEdge.V0 = vertIdx;
						}
					}
				}

				/* Add output edge to the skeleton. */
				bool bAllowAddEdge = true;
				for (FEdgeIndex & SkelEdge : GeomSkel.EdgeBuffer)
				{
					if (
						(SkelEdge.V0 == OutEdge.V0 && SkelEdge.V1 == OutEdge.V1)
						||
						(SkelEdge.V1 == OutEdge.V0 && SkelEdge.V0 == OutEdge.V1)
						)
					{
						bAllowAddEdge = false;
						break;
					}
				}

				if (bAllowAddEdge && OutEdge.IsValid()) GeomSkel.EdgeBuffer.Add(OutEdge);
			}

			pTempActor->Destroy();
			Geom.Skeleton = GeomSkel;

			/* Generate physics for geometry with the lowest accuracy. */
			Geom.GeneratePhysics(1, EGeometryCollisionGenerationType::GCGT_Box);

			StaticProceduralGeometry->Geometries[i] = Geom;
		}

		/* When static procedural geometry is done, we need to build geometries dependencies hierarchy. */
		StaticProceduralGeometry->BuildGeometriesHierarchy(SkeletonData);

		/* Lastly, we need to set up pivot points for each geometry. */
		StaticProceduralGeometry->BuildGeometriesPivots();

		/* We need to build each geometry skeleton normals. */
		StaticProceduralGeometry->BuildGeometriesSkeletonsNormals();
	}

	StaticProceduralGeometry->PostEditChange();

	FAssetRegistryModule::AssetCreated(StaticProceduralGeometry);
	StaticProceduralGeometry->MarkPackageDirty();


	if (bool bSuccess = UPackage::SavePackage(Package, StaticProceduralGeometry, EObjectFlags::RF_Public | EObjectFlags::RF_Standalone, *AssetFullPath))
	{
		UE_LOG(LogTemp, Warning, TEXT("Procedural Geometry: %s has been created."), *AssetName);
		return StaticProceduralGeometry;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Procedural Geometry: %s failed to create! Check your input asset paths."), *AssetName);
	}

	return nullptr;
}

FProceduralGeometrySkeleton UStaticProceduralGeometry::BuildTreeSkeletonData(const FString StringData)
{
	FProceduralGeometrySkeleton Skeleton;

	if (StringData.IsEmpty())
		return Skeleton;

	TArray<FString> Lines;
	StringData.ParseIntoArrayLines(Lines);

	int iBufferSelection = 0;
	int iSkinBufferIt = -1;
	for (const FString Line : Lines)
	{
		if (Line == "VertexBuffer:")
		{
			iBufferSelection = 1;
			continue;
		}
			
		else if (Line == "EdgesBuffer:")
		{
			iBufferSelection = 2;
			continue;
		}

		else if (Line == "SkinBuffer:")
		{
			iBufferSelection = 3;
			continue;
		}			

		if (iBufferSelection == 1)
		{
			FProceduralGeometrySkeletonVertex Vertex;

			FString X = Line;
			int32 x_idx = X.Find(";");
			X.RemoveAt(x_idx, X.Len() - x_idx);

			FString Y = Line;
			Y.RemoveAt(0, x_idx + 1);
			int32 y_idx = Y.Find(";");
			Y.RemoveAt(y_idx, Y.Len() - y_idx);

			FString Z = Line;
			Z.RemoveAt(0, x_idx + 1);
			Z.RemoveAt(0, y_idx + 1);

			Vertex.Position.X = -(FCString::Atof(*X) * 100.f);
			Vertex.Position.Y = FCString::Atof(*Y) * 100.f;
			Vertex.Position.Z = FCString::Atof(*Z) * 100.f;

			Skeleton.VertexBuffer.Add(Vertex);
		}
		else if (iBufferSelection == 2)
		{
			FEdgeIndex Edge;

			FString V0 = Line;
			int32 v0_idx = V0.Find(";");
			V0.RemoveAt(v0_idx, V0.Len() - v0_idx);

			FString V1 = Line;
			V1.RemoveAt(0, v0_idx + 1);

			Edge.V0 = FCString::Atoi(*V0);
			Edge.V1 = FCString::Atoi(*V1);

			Skeleton.EdgeBuffer.Add(Edge);
		}
		else if (iBufferSelection == 3)
		{
			iSkinBufferIt++;

			FProceduralGeometrySkeletonVertex SkelVert = Skeleton.VertexBuffer[iSkinBufferIt];

			FString X = Line;
			X.RemoveAt(0, 1);
			int32 x_idx = X.Find(",");
			X.RemoveAt(x_idx, X.Len() - x_idx);

			FString Y = Line;
			Y.RemoveAt(0, x_idx + 2);
			Y.RemoveAt(Y.Len() - 1, 1);
			
			SkelVert.Radius.X = FCString::Atof(*X) * 4.f;
			SkelVert.Radius.Y = FCString::Atof(*Y) * 4.f;

			Skeleton.VertexBuffer[iSkinBufferIt] = SkelVert;
		}
	}

	return Skeleton;
}

void UStaticProceduralGeometry::BuildStaticMeshPhysics(UStaticMesh *pSourceMesh)
{
	if (!IsValid(pSourceMesh))
		return;

	const FString AssetName = pSourceMesh->GetName() + TEXT("_procedural");
	const FString AssetPath = pSourceMesh->GetPathName().Replace(*FPackageName::GetLongPackageAssetName(pSourceMesh->GetPathName()), *AssetName);
	const FString AssetFullPath = FPaths::ConvertRelativePathToFull(FPaths::ProjectContentDir()) + AssetPath.Replace(TEXT("/Game/"), TEXT(""), ESearchCase::CaseSensitive) + FPackageName::GetAssetPackageExtension();
	const FString SkeletonPath = AssetFullPath.Replace(*(pSourceMesh->GetName() + TEXT("_procedural") + FPackageName::GetAssetPackageExtension()), *(pSourceMesh->GetName() + TEXT("_skel.fp")));
	FString SkeletonDataString;
	if (!FFileHelper::LoadFileToString(SkeletonDataString, *SkeletonPath))
	{
		UE_LOG(LogTemp, Error, TEXT("Skeleton: %s not found!"), *SkeletonPath);
		return;
	}
	FProceduralGeometrySkeleton Skeleton = BuildTreeSkeletonData(SkeletonDataString);

	pSourceMesh->BodySetup->AggGeom.BoxElems.Empty();
	pSourceMesh->BodySetup->AggGeom.SphereElems.Empty();
	pSourceMesh->BodySetup->AggGeom.SphylElems.Empty();
	pSourceMesh->BodySetup->AggGeom.TaperedCapsuleElems.Empty();
	pSourceMesh->BodySetup->AggGeom.ConvexElems.Empty();

	TArray<FKBoxElem> GeneratedBoxes = BuildPhysicsBoxesBySkeleton(Skeleton);

	for (const FKBoxElem & Box : GeneratedBoxes)
	{
		FKConvexElem Convex;
		Convex.ConvexFromBoxElem(Box);
		pSourceMesh->BodySetup->AggGeom.ConvexElems.Add(Convex);
	}

	pSourceMesh->Build(false);
	pSourceMesh->PostEditChange();
	FAssetRegistryModule::AssetCreated(pSourceMesh);
}

TArray<FKBoxElem> UStaticProceduralGeometry::BuildPhysicsBoxesBySkeleton(const FProceduralGeometrySkeleton & Skeleton)
{
	TArray<FKBoxElem> Boxes;

	/* Divide skeleton if higher accurarcy is needed. */
	FProceduralGeometrySkeleton UseSkeleton = Skeleton;
	DivideSkeleton(0, UseSkeleton);

	/* Iterate through each edge of the skeleton. */
	for (int32 edgeIdx = 0; edgeIdx < UseSkeleton.GetNumEdges(); edgeIdx++)
	{
		/* Get physics data for edge. */
		float Radius, HalfLength;
		FRotator Rotation;
		FVector Center;
		UseSkeleton.GetEdgeDataForPhysics(edgeIdx, Radius, HalfLength, Center, Rotation);

		/* Generate box. */
		FKBoxElem Box = FKBoxElem(Radius * 23.f, Radius * 23.f, HalfLength * 2.f);
		Box.Center = Center;
		Box.Rotation = Rotation;
		Boxes.Add(Box);
	}

	return Boxes;
}

void UStaticProceduralGeometry::DivideSkeleton(const int Division, FProceduralGeometrySkeleton & OutSkeleton)
{

	/* If division is less than 1, return. Skeleton remains unchanged */
	if (Division < 1)
		return;

	for (int div = 0; div < Division; div++)
	{
		FProceduralGeometrySkeleton NewSkeleton;
		/* Iterate through each skeleton edge in this geometry. */
		for (const FEdgeIndex & Edge : OutSkeleton.EdgeBuffer)
		{
			FProceduralGeometrySkeletonVertex V0 = OutSkeleton.VertexBuffer[Edge.V0];
			FProceduralGeometrySkeletonVertex V1 = OutSkeleton.VertexBuffer[Edge.V1];

			FProceduralGeometrySkeletonVertex V3 = FProceduralGeometrySkeletonVertex::InterpolateVertex(V0, V1, 0.5f);

			FEdgeIndex EdgeFirst, EdgeSecond;
			//
			int32 idx = NewSkeleton.FindVertex(V0);
			if (idx == INDEX_NONE)
				EdgeFirst.V0 = NewSkeleton.VertexBuffer.Add(V0);
			else
				EdgeFirst.V0 = idx;
			//
			EdgeSecond.V0 = EdgeFirst.V1 = NewSkeleton.VertexBuffer.Add(V3);
			//
			idx = NewSkeleton.FindVertex(V1);
			if (idx == INDEX_NONE)
				EdgeSecond.V1 = NewSkeleton.VertexBuffer.Add(V1);
			else
				EdgeSecond.V1 = idx;
			//
			NewSkeleton.EdgeBuffer.Add(EdgeFirst);
			NewSkeleton.EdgeBuffer.Add(EdgeSecond);
		}
		//
		OutSkeleton = NewSkeleton;
	}
}

void UStaticProceduralGeometry::BuildGeometriesHierarchy(const FProceduralGeometrySkeleton & FullSkeleton)
{
	/* This list will map Geometries with their associated list of edges within FullSkeleton. */
	TArray<TArray<FEdgeIndex>> EdgesArray;

	/* We need to iterate through each geometry. */
	for (int i = 0; i < Geometries.Num(); i++)
	{
		/* Store currently iterated geom. */
		const FProceduralGeometry & Geom = Geometries[i];

		/* This array will store edges that happen to be inside this geometry. */
		TArray<FEdgeIndex> Edges;

		/* We need to spawn actor with procedural geometry component in order to be able to perform raycasts. */
		FActorSpawnParameters Params;
		Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		AActor *pActor = GWorld->GetWorld()->SpawnActor<AActor>(AActor::StaticClass(), FVector(0.f, 0.f, -5000.f), FRotator(0.f, 0.f, 0.f), Params);
		UProceduralGeometryComponent *pComponent = NewObject<UProceduralGeometryComponent>(pActor);
		pComponent->RegisterComponent();
		pActor->SetRootComponent(pComponent);
		pActor->SetActorLocation(FVector(0.f, 0.f, -5000.f));
		pComponent->Add(Geom, false);
		pComponent->Update();

		/* Perform raycast from any given point towards each skeleton vertex to see which edges are contained within this geometry. */
		/* Note that the raycast must be performed from 6 sides, so 6 raycasts. The reason for it is that geometry might have some irregular shapes, and one sided raycast might return geometry, but vertex might be still outside, but on the other side of the geometry. */
		TArray<FVector> RayHitStart;
		RayHitStart.Add(FVector(5000.f, 0.f, -5000.f));
		RayHitStart.Add(FVector(-5000.f, 0.f, -5000.f));
		RayHitStart.Add(FVector(0.f, 5000.f, -5000.f));
		RayHitStart.Add(FVector(0.f, -5000.f, -5000.f));
		RayHitStart.Add(FVector(0.f, 0.f, -10000.f));
		RayHitStart.Add(FVector(0.f, 0.f, 0.f));
		const FVector V = pActor->GetActorLocation();

		/* Iterate through each edge in the full skeleton to see which gometry contains at least one vertex of which edge. */
		for (const FEdgeIndex & Edge : FullSkeleton.EdgeBuffer)
		{
			/* Store global locations of both ends of the edge. */
			const FVector V0 = V + FullSkeleton.VertexBuffer[Edge.V0].Position;
			const FVector V1 = V + FullSkeleton.VertexBuffer[Edge.V1].Position;

			/* Six iterations mean raycasts from six sides. Firstly we need to check if all iterations return the same result. If one raycast fails, it means vertex is not inside this geometry. */
			/* If this ray counter equals 6, then edge is inside. */
			int RayCounter = 0;

			for (int it = 0; it < 6; it++)
			{
				/* Perform actual raycasts. */
				FHitResult HitResult;
				FCollisionQueryParams CollisionQueryParams;
				CollisionQueryParams.bTraceComplex = true;
				if (pComponent->LineTraceComponent(HitResult, RayHitStart[it], V0, CollisionQueryParams) || pComponent->LineTraceComponent(HitResult, RayHitStart[it], V1, CollisionQueryParams))
					RayCounter++;
				/* One fail is enough for saying that vertex is not inside the geometry, that's why there is no need to further process this loop if at least one fails. */
				else break;
			}
			/* If ray counter equals 6, it means that edge can be safely added to this geometry edges. */
			if (RayCounter == 6)
				Edges.Add(Edge);
		}

		/* Dismiss actor. */
		pActor->Destroy();

		/* When edges loop is done, map current geometry with found edges. If edges happen to be empty, we can show warning, because there is definitely something wrong with either raycast or skeleton. */
		if (Edges.Num() < 1)
		{
			UE_LOG(LogTemp, Warning, TEXT("Edges for one of procedural geometries are empty! Check your raycast function or skeleton."));
			return;
		}
		else
		{
			EdgesArray.Add(Edges);
		}
	}

	/* Store root geometry temporarily. */
	FProceduralGeometry RootGeom;
	int32 RootGeomIdx = -1;

	/* We need to find which geometery is root. */
	/* In order to do that, we need to iterate through each geometry and see which one's bounding box includes 0 vector. */
	for (const FProceduralGeometry & Geom : Geometries)
	{
		RootGeomIdx++;

		if (Geom.LocalBounds.IsInside(FVector(0.f, 0.f, 0.f)))
		{
			RootGeometryID = Geom.GetID();
			RootGeom = Geom;
			break;
		}
	}
	
	/* If no root geometry was found, show warning, and return here. */
	if (!RootGeometryID.IsIDValid())
		return;

	TArray<int32> ParentGeoms;
	ParentGeoms.Add(RootGeomIdx);
	TArray<int32> AllGeoms;
	for (int i = 0; i < Geometries.Num(); i++)
	{
		if (i == RootGeomIdx)
			continue;;

		AllGeoms.Add(i);
	}
	BuildGeometriesHierarchy(EdgesArray, ParentGeoms, AllGeoms);
}

void UStaticProceduralGeometry::BuildGeometriesHierarchy(const TArray<TArray<FEdgeIndex>> & EdgeList, TArray<int32> ParentGeoms, TArray<int32> AllGeoms)
{
	/* List of parent geoms that needs to be passed for recursive execution further. */
	TArray<int32> NewParentGeoms;
	/* List of all geoms that can take a part in children creation potentially. */
	TArray<int32> NewAllGeoms = AllGeoms;

	/* Iterate through all parent geoms. */
	for (int32 i = 0; i < ParentGeoms.Num(); i++)
	{
		/* This is index actually used. */
		const int32 ParentGeomIdx = ParentGeoms[i];

		/* Iterate through all geoms. */
		for (int32 j = 0; j < AllGeoms.Num(); j++)
		{
			/* This is actually index of potential children to use. */
			const int32 GeomIdx = AllGeoms[j];

			/* Check if parent geom and geom share edge. */
			bool SharesEdge = false;

			for (const FEdgeIndex & ParentEdge : EdgeList[ParentGeomIdx])
			{
				for (const FEdgeIndex & GeomEdge : EdgeList[GeomIdx])
				{
					if (ParentEdge == GeomEdge)
					{
						SharesEdge = true;
						break;
					}
				}
				if (SharesEdge)
					break;
			}

			/* Set up child for parent if share edge. */
			if (SharesEdge)
			{
				Geometries[ParentGeomIdx].AddChildGeometryID(Geometries[GeomIdx].GetID());
				/* This geom is going to be a parent in further recursion, add it to new parents list. */
				NewParentGeoms.Add(GeomIdx);
				/* Also remove it from NewAllGeoms, so it is not searched through in next recursive pass. */
				NewAllGeoms.Remove(GeomIdx);
			}
		}
	}

	/* Begin another recursive pass if both NewParentGeoms and NewAllGeoms are not empty. */
	if (NewParentGeoms.Num() > 0 && NewAllGeoms.Num() > 0)
		BuildGeometriesHierarchy(EdgeList, NewParentGeoms, NewAllGeoms);
}

void UStaticProceduralGeometry::BuildGeometriesPivots()
{
	/* To build pivots, we need to obviously first iterate through each geometry. */
	for (FProceduralGeometry & Geom : Geometries)
	{
		/* If geometry is root, then its pivot must be skeleton vertex that's closest to 0, and make it 0 as well. */
		if (Geom.GetID() == RootGeometryID)
		{
			/* Store currently closest to 0 skeleton vertex. */
			int32 ClosestZeroVertIdx = 0;
			for(int32 vertIdx = 1; vertIdx < Geom.Skeleton.VertexBuffer.Num(); vertIdx++)
			{
				const FVector LastPos = Geom.Skeleton.VertexBuffer[ClosestZeroVertIdx].Position;
				const FVector CurPos = Geom.Skeleton.VertexBuffer[vertIdx].Position;
				const float LastDist = FVector::Dist(FVector(0.f, 0.f, 0.f), LastPos);
				const float CurDist = FVector::Dist(FVector(0.f, 0.f, 0.f), CurPos);

				if (CurDist < LastDist)
					ClosestZeroVertIdx = vertIdx;
			}

			Geom.Skeleton.VertexBuffer[ClosestZeroVertIdx].Position = FVector(0.f, 0.f, 0.f);
			Geom.Pivot = ClosestZeroVertIdx;
			continue;
		}

		/* If geometry is not root, we need to set up pivot to be simply one of the ending skeleton vertex for that specific geometry. */
		/* For this example, we will take vertex that is closest to the point 0, which is the root pivot. TODO: In the future, user might need to define which point in the geometry should be its pivot, or do some few enum standards to choose from. */
		/* End skeleton vertexes are those that are used only once throughout all edges buffer. Those that exist in only one edge. */

		/* First of all store end vertex indexes. */
		TArray<int32> EndVertIndexes;

		/* Iterate through skeleton vertexes. */
		for (int32 vertIdx = 0; vertIdx < Geom.Skeleton.VertexBuffer.Num(); vertIdx++)
		{
			/* Store occurences of iterated vertex. */
			int32 vertOccurences = 0;

			/* Iterate through each skeleton edge. */
			for (const FEdgeIndex & Edge : Geom.Skeleton.EdgeBuffer)
			{
				/* Check if either of the edge end matches vertIdx. If so, then vertex occurences should be incremented. */
				if (Edge.V0 == vertIdx || Edge.V1 == vertIdx)
					vertOccurences++;
			}

			/* Now check the vertOccurences. If it equals 1, it means we are dealing with skeleton end vertex, so add it to end vertexes buffer. */
			if (vertOccurences == 1)
				EndVertIndexes.Add(vertIdx);
		}

		/* If end verts array is empty, something is definitely wrong here, so inform user, and return from here. */
		if (EndVertIndexes.Num() < 1)
		{
			UE_LOG(LogTemp, Warning, TEXT("Geometry skeleton end vertexes were not found!"));
			return;
		}

		/* Only two the furthest away from each other vertexes are considerent end vertexes. Find them. */
		TArray<int32> is;
		TArray<int32> js;
		TMap<int32, float> dist;
		int32 id = -1;
		for (int32 i = 0; i < EndVertIndexes.Num(); i++)
		{
			const FVector ipos = Geom.Skeleton.VertexBuffer[EndVertIndexes[i]].Position;
			for (int32 j = i + 1; j < EndVertIndexes.Num(); j++)
			{
				id++;
				const FVector jpos = Geom.Skeleton.VertexBuffer[EndVertIndexes[j]].Position;

				const float d = FVector::Dist(ipos, jpos);
				is.Add(EndVertIndexes[i]);
				js.Add(EndVertIndexes[j]);
				dist.Emplace(id, d);
			}
		}

		/* Sort by distance. */
		dist.ValueSort([](const float A, const float B)
		{
			return A > B;
		});

		TArray<int32> ids;
		dist.GenerateKeyArray(ids);

		/* First id in ids list is the pair that is the furthest from each other. */
		const FProceduralGeometrySkeletonVertex & V0 = Geom.Skeleton.VertexBuffer[is[ids[0]]];
		const FProceduralGeometrySkeletonVertex & V1 = Geom.Skeleton.VertexBuffer[js[ids[0]]];

		/* Check which one is closest to 0 point and make it pivot. */
		if (FVector::Dist(V0.Position, FVector(0.f, 0.f, 0.f)) < FVector::Dist(V1.Position, FVector(0.f, 0.f, 0.f)))
			Geom.Pivot = is[ids[0]];
		else
			Geom.Pivot = js[ids[0]];

		/* Now store the last closest found vertex to point 0. The vertex to start from is the first in EndVertIndexes. */
		//int32 ClosestVertIdx = EndVertIndexes[0];

		///* Iterate through all end vertexes found, except the first one because it has already been assigned. */
		//for( int32 vertIdx = 1; vertIdx < EndVertIndexes.Num(); vertIdx++)
		//{
		//	/* Compare current closest vertex to the iterated one, and if iterated one is closer to the 0 vector, make it the current closest. */
		//	const float curDist = FVector::Dist(Geom.Skeleton.VertexBuffer[ClosestVertIdx].Position, FVector(0.f, 0.f, 0.f));
		//	const float itDist = FVector::Dist(Geom.Skeleton.VertexBuffer[EndVertIndexes[vertIdx]].Position, FVector(0.f, 0.f, 0.f));

		//	/* If itDist is smaller than curDist, then make it current closest. */
		//	if (itDist < curDist)
		//		ClosestVertIdx = EndVertIndexes[vertIdx];
		//}

		///* Now ClosestVertIdx specifies the closest geometry skeleton vertex index to the root pivot point(0). This vertex location becomes this geometry pivot. */
		//Geom.Pivot = ClosestVertIdx;
	}
}

void UStaticProceduralGeometry::BuildGeometriesSkeletonsNormals()
{
	/* First of all, we will sort edges by their distance from geometry pivot point. */
	/*
		By saying edges, we take under consideration both end of each edge and check
		which end is closer to the geometry pivot point. Then the closer one becomes V0 and further one V1.
		Second step is to sort edges by the distance to pivot. And this is achieved by simply checking each edge's V0
		and comparing their distances. Then result is saved into the skeleton.
	*/
	/* Iterate over each geometry by reference, so we can directly modify them. */
	for (FProceduralGeometry & Geom : Geometries)
	{
		/* Store skeleton reference to be able to modify it. */
		FProceduralGeometrySkeleton & Skel = Geom.Skeleton;

		/* Iterate through each skeleton edge. */
		for (FEdgeIndex & Edge : Skel.EdgeBuffer)
		{
			/* Measure distance between each end of this edge and geometry pivot point. */
			const float dist_v0 = FVector::Dist(Geom.GetPivotLocation(), Skel.VertexBuffer[Edge.V0].Position);
			const float dist_v1 = FVector::Dist(Geom.GetPivotLocation(), Skel.VertexBuffer[Edge.V1].Position);

			/* If dist_v1 is less than dist_v0, then swap these ends. */
			if (dist_v1 < dist_v0)
				Edge.FlipEdge();
		}

		/* Now that the edges are properly flipped towards pivot, we can sort edges by their V0 distance. */
		/* Store index of the edges mapped to their distances. */
		TMap<int32, float> EdgeIdxToDist;

		/* So Iterate over edges again. */
		for (int32 edgeIdx = 0; edgeIdx < Skel.EdgeBuffer.Num(); edgeIdx++)
		{
			/* Store edge reference. */
			const FEdgeIndex & Edge = Skel.EdgeBuffer[edgeIdx];

			/* Measure edge's V0 distance to pivot and map it. */
			EdgeIdxToDist.Emplace(edgeIdx, FVector::Dist(Geom.GetPivotLocation(), Skel.VertexBuffer[Edge.V0].Position));
		}

		/* Sort EdgeIdxToDist by the distances, so we get the list of edges in the proper line. */
		EdgeIdxToDist.ValueSort([](const float A, const float B)
		{
			return A < B;
		});

		/* Now we need to find out which vertex is used how many times. */
		/* Store map of the vertex index to array of edges that use it. */
		TMap<int32, TArray<int32>> VertIdxToOccurAsV0;
		TMap<int32, TArray<int32>> VertIdxToOccurAsV1;

		/* Iterate through each vertex, because they are unique and lopp will be shorter therefore. */
		for (int32 vertIdx = 0; vertIdx < Skel.VertexBuffer.Num(); vertIdx++)
		{
			/* Store actual vertex. */
			const FProceduralGeometrySkeletonVertex & Vert = Skel.VertexBuffer[vertIdx];

			/* Store vertex edge index occurence array. */
			TArray<int32> EdgeIndexesV0;
			TArray<int32> EdgeIndexesV1;

			/* Iterate through each edge. */
			for (int32 edgeIdx = 0; edgeIdx < Skel.EdgeBuffer.Num(); edgeIdx++)
			{
				/* Store actual edge. */
				const FEdgeIndex & Edge = Skel.EdgeBuffer[edgeIdx];

				/* If this edge's V0 equals this vertIdx. */
				if (Edge.V0 == vertIdx)
				{
					/* Then emplace this vertex occurence. */
					EdgeIndexesV0.Add(edgeIdx);
				}
				/* Otherwise do the same check with edge's V1. */
				else if (Edge.V1 == vertIdx)
				{
					EdgeIndexesV1.Add(edgeIdx);
				}
			}

			/* Now add these occurences to map. */
			if(EdgeIndexesV0.Num() > 0) VertIdxToOccurAsV0.Emplace(vertIdx, EdgeIndexesV0);
			if(EdgeIndexesV1.Num() > 0) VertIdxToOccurAsV1.Emplace(vertIdx, EdgeIndexesV1);
		}

		/* Turn EdgeIdxToDist into array, because we do not need distances anymore. */
		TArray<int32> EdgeIdxSortedList;
		EdgeIdxToDist.GenerateKeyArray(EdgeIdxSortedList);

		/* At this point edges are sorted properly, we can begin building normals. */
		/* Iterate through each vertex in the skeleton. Because vertexes never repeat, it is better to iterate through them than through edges. */
		for (int32 vertIdx = 0; vertIdx < Skel.VertexBuffer.Num(); vertIdx++)
		{
			/* Store current vertex reference. Vertex iterated will be the vertex that is actually modified. */
			FProceduralGeometrySkeletonVertex & Vert = Skel.VertexBuffer[vertIdx];

			/* Now we need to handle various scenarios. */
			/* Store arrays of this vector's occurences as V0 and V1. */
			TArray<int32> * V0 = VertIdxToOccurAsV0.Find(vertIdx);
			TArray<int32> * V1 = VertIdxToOccurAsV1.Find(vertIdx);

			/* For each edge for which this vertex is V0. */
			if(V0 != nullptr)
			{
				/* We will blend between all locations of the edges where this vertex is V0 and find the middle point possible. */
				/* Store interpolated location here, and for the beginning assign first location of the V1. */
				FVector BlendLoc = Skel.VertexBuffer[Skel.EdgeBuffer[(*V0)[0]].V1].Position;
				
				/* Iterate through all edges that consider this vertex as V0. */
				for (int32 EdgeIdx : (*V0))
				{
					/* Store actual edge. */
					FEdgeIndex & Edge = Skel.EdgeBuffer[EdgeIdx];

					/* Blend between BlendLoc and current edge V1 location. */
					BlendLoc = FMath::Lerp(BlendLoc, Skel.VertexBuffer[Edge.V1].Position, 0.5f);
				}

				/* At this point blend location is found, so we can get normal direction between BlendLoc and Vert.Position and assign it to Vert. */
				Vert.Normal = (BlendLoc - Vert.Position).GetSafeNormal();
			}
			/* If this is not V0 for any edge, but is V1 for some. */
			else if(V1 != nullptr)
			{
				/* Then blend between all locations of edges where this vertex is V1 and find the middle point possible. */
				/* Store interpolated location here, and for beginning assign first location of the V1. */
				FVector BlendLoc = Skel.VertexBuffer[Skel.EdgeBuffer[(*V1)[0]].V0].Position;

				/* Iterate through all edges that consider this vertex as V1. */
				for (int32 EdgeIdx : (*V1))
				{
					/* Store actual edge. */
					FEdgeIndex & Edge = Skel.EdgeBuffer[EdgeIdx];

					/* Blend betweeen BlendLoc and current edge V0 location. */
					BlendLoc = FMath::Lerp(BlendLoc, Skel.VertexBuffer[Edge.V0].Position, 0.5f);
				}

				/* At this point blend location is found, so we can get normal direction between Vert.Position and BlendLoc and assign it to Vert. */
				Vert.Normal = (Vert.Position - BlendLoc).GetSafeNormal();
			}
			/* We also need to handle possibility, if this vertex is V1 for some edge or edges, but also this edge's V0 is used more than 1 time, then it should inherit Normal from that V0. */
			if (V1 != nullptr)
			{
				/* Grab first occurence of the edge this vertex takes part it. */
				const FEdgeIndex & Edge = Skel.EdgeBuffer[(*V1)[0]];

				/* Grab V0 vertex of that edge. */
				const int32 vertIdxV0 = Edge.V0;

				/* Check if this vertex is used more than 1 time as V0. */
				TArray<int32> * OccurAsV0 = VertIdxToOccurAsV0.Find(vertIdxV0);
				if (OccurAsV0 != nullptr && OccurAsV0->Num() > 1)
				{
					/* Then this vertex normal should equal this found V0 normal. */
					Vert.Normal = Skel.VertexBuffer[vertIdxV0].Normal;
				}
			}
		}
	}
}

bool UStaticProceduralGeometry::GetGeometryByID(const FProceduralGeometryID SearchID, FProceduralGeometry & FoundGeometry)
{
	if (!SearchID.IsIDValid())
		return false;

	for (FProceduralGeometry & Geom : Geometries)
	{
		if (Geom.GetID() == SearchID)
		{
			FoundGeometry = Geom;
			return true;
		}
	}
	return false;
}

bool UStaticProceduralGeometry::GetRootGeometry(FProceduralGeometry & RootGeom)
{
	return GetGeometryByID(RootGeometryID, RootGeom);
}