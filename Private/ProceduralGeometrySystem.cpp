// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ProceduralGeometrySystem.h"
#include "AssetTools/Public/AssetToolsModule.h"
#include "ProceduralGeometrySystemFactory.h"
#include "StaticProceduralGeometry.h"

#define LOCTEXT_NAMESPACE "FProceduralGeometrySystemModule"

void FProceduralGeometrySystemModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
}

void FProceduralGeometrySystemModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FProceduralGeometrySystemModule, ProceduralGeometrySystem)