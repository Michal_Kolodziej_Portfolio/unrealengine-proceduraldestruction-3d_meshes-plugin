#include "ProceduralGeometrySystemFactory.h"
#include "StaticProceduralGeometry.h"
#include "AssetTypeCategories.h"

UStaticProceduralGeometryFactory::UStaticProceduralGeometryFactory(const FObjectInitializer & ObjectInitializer)
	: Super(ObjectInitializer)
{
	bCreateNew = true;
	bEditAfterNew = true;
	SupportedClass = UStaticProceduralGeometry::StaticClass();
}

uint32 UStaticProceduralGeometryFactory::GetMenuCategories() const
{
	return EAssetTypeCategories::Gameplay;
}

UObject * UStaticProceduralGeometryFactory::FactoryCreateNew(UClass * InClass, UObject * InParent, FName InName, EObjectFlags Flags, UObject * Context, FFeedbackContext * Warn)
{
	UStaticProceduralGeometry * StaticProceduralGeometry = NewObject<UStaticProceduralGeometry>(InParent, InClass, InName, Flags);
	return StaticProceduralGeometry;
}
