#include "ProceduralGeometry.h"
#include "GeomTools.h"

FProceduralGeometry::FProceduralGeometry()
{
	LocalBounds.Init();
	Pivot = -1;
}

bool FProceduralGeometry::CreateProceduralGeometry(const TArray<FProceduralGeometryVertex> Vertexes, const TArray<uint32> Indexes, const FBox LocalBounds, UMaterialInterface * Material, const bool Visible, FProceduralGeometry & OutGeometry)
{
	OutGeometry.VertexBuffer = Vertexes;
	OutGeometry.IndexBuffer = Indexes;
	OutGeometry.Material = Material;
	OutGeometry.GeometryVisible = Visible;
	OutGeometry.LocalBounds = LocalBounds;
	return true;
}

bool FProceduralGeometry::CreateProceduralGeometry(UMaterialInterface * Material, const bool Visible, const TArray<FProceduralGeometryID>& Children, const FProceduralGeometryID & InitId, FProceduralGeometry & OutGeometry)
{
	OutGeometry.Material = Material;
	OutGeometry.GeometryVisible = Visible;
	OutGeometry.ChildrenIDs = Children;
	OutGeometry.ID = InitId;
	return true;
}

TArray<FProceduralGeometry> FProceduralGeometry::CreateProceduralGeometriesFromStaticMesh(UStaticMesh * StaticMesh, const bool CloseHoles, const FColor HoleCapsVertexColors, const bool GeometrizeUVCuts)
{
	if (!IsValid(StaticMesh))
		return TArray<FProceduralGeometry>();

	/* TODO: Solve storing all LODs */
	TArray<FProceduralGeometry> Geometries;
	const int LODIndex = 0;

	if (StaticMesh->RenderData != nullptr && StaticMesh->RenderData->LODResources.IsValidIndex(LODIndex))
	{
		const FStaticMeshLODResources & LOD = StaticMesh->RenderData->LODResources[LODIndex];

		for (int32 SectionIdx = 0; SectionIdx < StaticMesh->GetNumSections(LODIndex); SectionIdx++)
		{
			if (LOD.Sections.IsValidIndex(SectionIdx))
			{
				FProceduralGeometry Geom;
				
				Geom.Material = StaticMesh->GetMaterial(SectionIdx);
				Geom.GeometryVisible = true;

				const FStaticMeshSection & StaticSection = LOD.Sections[SectionIdx];

				for (int IndiceIdx = 0; IndiceIdx < LOD.IndexBuffer.GetNumIndices(); IndiceIdx++)
				{
					Geom.IndexBuffer.Add(LOD.IndexBuffer.GetIndex(IndiceIdx));
				}

				for (uint32 VertIdx = 0; VertIdx < LOD.VertexBuffers.PositionVertexBuffer.GetNumVertices(); VertIdx++)
				{
					const FVector pos = LOD.VertexBuffers.PositionVertexBuffer.VertexPosition(VertIdx);
					const FVector nor = LOD.VertexBuffers.StaticMeshVertexBuffer.VertexTangentZ(VertIdx);
					const FProceduralGeometryTangent tan = FProceduralGeometryTangent(LOD.VertexBuffers.StaticMeshVertexBuffer.VertexTangentX(VertIdx), LOD.VertexBuffers.StaticMeshVertexBuffer.VertexTangentX(VertIdx).W < 0.f);
					const FVector2D uv = FVector2D(LOD.VertexBuffers.StaticMeshVertexBuffer.GetVertexUV(VertIdx, 0));
					FProceduralGeometryVertex vert;
					vert.Position = pos;
					vert.Normal = nor;
					vert.Tangent = tan;
					vert.UV0 = uv;
					Geom.VertexBuffer.Add(vert);
					Geom.LocalBounds += pos;
				}

				if (Geom.VertexBuffer.Num() > 3 && GeometrizeUVCuts)
				{
					TArray<FProceduralGeometry> SubGeoms = CreateProceduralGeometriesByUVCuts(Geom, CloseHoles, HoleCapsVertexColors);
					for (FProceduralGeometry SubGeom : SubGeoms)
					{
						Geometries.Add(SubGeom);
					}
				}
				else
					Geometries.Add(Geom);
			}
		}
	}

	return Geometries;
}

TArray<FProceduralGeometry> FProceduralGeometry::CreateProceduralGeometriesByUVCuts(FProceduralGeometry Geometry, const bool CloseHoles, const FColor HoleCapsVertexColors)
{
	TArray<FProceduralGeometry> Geometries;

	TArray<TArray<uint32>> UVGeometries;
	TArray<FVector> Vertices = Geometry.GetVertexPositions();
	TArray<uint32> Triangles = Geometry.IndexBuffer;
	TArray<FVector> Normals = Geometry.GetVertexNormals();
	TArray<FVector2D> UVs = Geometry.GetUVs();
	TArray<FProceduralGeometryTangent> Tangents = Geometry.GetVertexTangents();
	TArray<FColor> Colors = Geometry.GetVertexColors();

	for (int32 vertTriIdx = 0; vertTriIdx < Triangles.Num(); vertTriIdx += 3)
	{
		/* Store currently processed triangle. */
		TArray<uint32> Triangle = { Triangles[vertTriIdx], Triangles[vertTriIdx + 1], Triangles[vertTriIdx + 2] };

		/* Store triangle UVs. */
		TArray<FVector2D> TriangleUVs = { UVs[Triangle[0]], UVs[Triangle[1]], UVs[Triangle[2]] };

		/* Store appearance index of the triangle in section. */
		TArray<int32> TriangleSectionsAppearance;

		/* Iterate through section uv sections that has been already saved, if. */
		for (int32 uvSectionIdx = 0; uvSectionIdx < UVGeometries.Num(); uvSectionIdx++)
		{
			/* Store uv section checked. */
			TArray<uint32> UVSection = UVGeometries[uvSectionIdx];

			/* Flag that defines whether uv section was assigned already. */
			bool Assigned = false;

			/* Iterate through  each uv section vertex. */
			for (int32 sectionVertIdx = 0; sectionVertIdx < UVSection.Num(); sectionVertIdx++)
			{
				/* Iterate through each triangle vertex to see if they match. */
				for (int32 triangleVertIdx = 0; triangleVertIdx < 3; triangleVertIdx++)
				{
					/* Compare current triangle vertex with section vertex. */
					if (TriangleUVs[triangleVertIdx] == UVs[UVSection[sectionVertIdx]])
					{
						/* If triangle was already assigned to this group, don't do it again. If not, assign it. */
						if (!TriangleSectionsAppearance.Contains(uvSectionIdx))
							TriangleSectionsAppearance.Add(uvSectionIdx);

						/* Mark assigned. */
						Assigned = true;

						/* Break the loop so it doesn't process any unnecessary calculations. */
						break;
					}
				}
				/* As triangle has been assigned, no further vertexes need to be checked for this group. */
				if (Assigned)
					break;
			}
		}

		/* If triangle not found in any section - create new one. */
		if (TriangleSectionsAppearance.Num() == 0)
		{
			TArray<uint32> NewSection = { Triangle[0], Triangle[1], Triangle[2] };
			UVGeometries.Add(NewSection);
		}

		/* If triangle found in one section only, add it there. */
		else if (TriangleSectionsAppearance.Num() == 1)
		{
			UVGeometries[TriangleSectionsAppearance[0]].Add(Triangle[0]);
			UVGeometries[TriangleSectionsAppearance[0]].Add(Triangle[1]);
			UVGeometries[TriangleSectionsAppearance[0]].Add(Triangle[2]);
		}

		/* If triangle found in more than one section, add it there and sort doubled up sections. */
		else
		{
			UVGeometries[TriangleSectionsAppearance[0]].Add(Triangle[0]);
			UVGeometries[TriangleSectionsAppearance[0]].Add(Triangle[1]);
			UVGeometries[TriangleSectionsAppearance[0]].Add(Triangle[2]);

			/* Iterate through all sections where triangle was found(except the first one, as it will store it), and merge them into that first one. */
			for (int32 sectionIdx = 1; sectionIdx < TriangleSectionsAppearance.Num(); sectionIdx++)
			{
				/* Store section actual index for easier use. */
				const int32 sectionIndex = TriangleSectionsAppearance[sectionIdx];

				/* Append currently iterated section to the first section. */
				UVGeometries[TriangleSectionsAppearance[0]].Append(UVGeometries[sectionIndex]);

				/* Delete this found section from array. TODO: This might not keep indexes, so above usage of it might crash. */
				UVGeometries.RemoveAt(sectionIndex);
			}
		}
	}

	/* Iterate through already sorted uv generated sections. */
	for (int32 sectionIdx = 0; sectionIdx < UVGeometries.Num(); sectionIdx++)
	{
		/* Store section mesh data. */
		TArray<FVector> GeometryVertexPositions;
		TArray<uint32> GeometryIndices;
		TArray<FVector> GeometryVertexNormals;
		TArray<FVector2D> GeometryVertexUVs;
		TArray<FProceduralGeometryTangent> GeometryVertexTangents;
		TMap<uint32, uint32> BaseVertToNewVertIdx;

		/* Iterate through each triangle in the section. Three vertexes at the time. */
		for (int32 vertIdx = 0; vertIdx < UVGeometries[sectionIdx].Num(); vertIdx += 3)
		{
			/* Store index for each vertex of this triangle inside source section. */
			const int32 SourceVertIdxA = UVGeometries[sectionIdx][vertIdx];
			const int32 SourceVertIdxB = UVGeometries[sectionIdx][vertIdx + 1];
			const int32 SourceVertIdxC = UVGeometries[sectionIdx][vertIdx + 2];

			////////////////////////////////
			uint32 TargetVertIdxA = 0;
			uint32 *Found_A = BaseVertToNewVertIdx.Find(SourceVertIdxA);
			if (Found_A == nullptr)
			{
				TargetVertIdxA = GeometryVertexPositions.Add(Vertices[SourceVertIdxA]);
				GeometryVertexNormals.Add(Normals[SourceVertIdxA]);
				GeometryVertexTangents.Add(Tangents[SourceVertIdxA]);
				GeometryVertexUVs.Add(UVs[SourceVertIdxA]);
				BaseVertToNewVertIdx.Emplace(SourceVertIdxA, TargetVertIdxA);
			}
			else
			{
				TargetVertIdxA = *Found_A;
			}

			////////////////////////////////
			uint32 TargetVertIdxB = 0;
			uint32 *Found_B = BaseVertToNewVertIdx.Find(SourceVertIdxB);
			if (Found_B == nullptr)
			{
				TargetVertIdxB = GeometryVertexPositions.Add(Vertices[SourceVertIdxB]);
				GeometryVertexNormals.Add(Normals[SourceVertIdxB]);
				GeometryVertexTangents.Add(Tangents[SourceVertIdxB]);
				GeometryVertexUVs.Add(UVs[SourceVertIdxB]);
				BaseVertToNewVertIdx.Emplace(SourceVertIdxB, TargetVertIdxB);
			}
			else
			{
				TargetVertIdxB = *Found_B;
			}
			////////////////////////////////
			uint32 TargetVertIdxC = 0;
			uint32 *Found_C = BaseVertToNewVertIdx.Find(SourceVertIdxC);
			if (Found_C == nullptr)
			{
				TargetVertIdxC = GeometryVertexPositions.Add(Vertices[SourceVertIdxC]);
				GeometryVertexNormals.Add(Normals[SourceVertIdxC]);
				GeometryVertexTangents.Add(Tangents[SourceVertIdxC]);
				GeometryVertexUVs.Add(UVs[SourceVertIdxC]);
				BaseVertToNewVertIdx.Emplace(SourceVertIdxC, TargetVertIdxC);
			}
			else
			{
				TargetVertIdxC = *Found_C;
			}
			////////////////////////////////
			GeometryIndices.Add(TargetVertIdxA);
			GeometryIndices.Add(TargetVertIdxB);
			GeometryIndices.Add(TargetVertIdxC);
		}

		/* Generate actual destructible mesh section for this uv-generated section. */
		FProceduralGeometry NewGeometry;
		if (CreateProceduralGeometryFromData(GeometryVertexPositions, GeometryIndices, GeometryVertexNormals, GeometryVertexUVs, GeometryVertexTangents, Geometry.Material, true, NewGeometry))
		{
			if (CloseHoles)
				FillProceduralGeometryHoles(NewGeometry, HoleCapsVertexColors);

			Geometries.Add(NewGeometry);
		}
			
	}

	if (Geometries.Num() == 0)
		Geometries.Add(Geometry);

	return Geometries;
}

void FProceduralGeometry::FillProceduralGeometryHoles(FProceduralGeometry & Geometry, const FColor HoleCapsVertexColors)
{
	/* First of all, we need to find boundary edges. */
/* They will be stored below. */
	TArray<TArray<FUtilEdge3D>> HolesEdges;
	TArray<FProceduralGeometryPolygonEdge> BoundaryEdges;

	/* Iterate through all triangles in the geometry. 3 vertexes at a time. */
	for (int32 vertTriIdx = 0; vertTriIdx < Geometry.IndexBuffer.Num(); vertTriIdx += 3)
	{
		/* Store currently processed triangle. */
		TArray<uint32> SearchTriangle = { Geometry.IndexBuffer[vertTriIdx], Geometry.IndexBuffer[vertTriIdx + 1], Geometry.IndexBuffer[vertTriIdx + 2] };

		/* Setup and store edges of currently processed triangle. */
		TArray<FProceduralGeometryPolygonEdge> SearchEdges;
		/* To set up edges, iterate through section vertexes. */
		for (int32 triIdx = 0; triIdx < 3; triIdx++)
		{
			FProceduralGeometryPolygonEdge Edge;
			Edge.V0 = Geometry.VertexBuffer[SearchTriangle[triIdx % 3]].Position;
			Edge.V1 = Geometry.VertexBuffer[SearchTriangle[(triIdx + 1) % 3]].Position;
			/* Add created edge to triangle edges list. */
			SearchEdges.Add(Edge);
		}

		/* Now that the currently iterated triangle is prepared, we can iterate through the triangles again to find matches. */
		/* Boundary edges are found by checking how many edges in the mesh are shared by more than 1 triangle. If edge exists only once, it is boundary edge. */
		for (int32 searchTriIdx = 0; searchTriIdx < Geometry.IndexBuffer.Num(); searchTriIdx += 3)
		{
			/* Store found triangle. */
			TArray<uint32> FoundTriangle = { Geometry.IndexBuffer[searchTriIdx], Geometry.IndexBuffer[searchTriIdx + 1], Geometry.IndexBuffer[searchTriIdx + 2] };

			/* Make sure that the triangle doesn't look up itself. */
			if (SearchTriangle[0] == FoundTriangle[0] && SearchTriangle[1] == FoundTriangle[1] && SearchTriangle[2] == FoundTriangle[2])
				continue;

			/* If there are no more search edges, break the loop. */
			if (SearchEdges.Num() < 1)
				break;

			/* Same as above, store and process found triangle edges. */
			TArray<FProceduralGeometryPolygonEdge> FoundEdges;
			for (int32 triIdx = 0; triIdx < 3; triIdx++)
			{
				FProceduralGeometryPolygonEdge Edge;
				Edge.V0 = Geometry.VertexBuffer[FoundTriangle[triIdx % 3]].Position;
				Edge.V1 = Geometry.VertexBuffer[FoundTriangle[(triIdx + 1) % 3]].Position;
				/* Add created edge to found edges list. */
				FoundEdges.Add(Edge);
			}

			/* Actual checking starts here. Test if any of the search and found edges match. */
			for (int32 foundEdgeIdx = 0; foundEdgeIdx < 3; foundEdgeIdx++)
			{
				/* Second loop cannot end at 2, although there are always three edges, because we will be removing edges from search buffer. */
				for (int32 searchEdgeIdx = 0; searchEdgeIdx < SearchEdges.Num(); searchEdgeIdx++)
				{
					/* Check edges in both directions, as V0 might be and most certainly will be V1 and V1 will be V0, but check both scenarios just in case. */
					if (FoundEdges[foundEdgeIdx].Equals(SearchEdges[searchEdgeIdx]))
					{
						/* If edge was found to be used by two triangles, remove it from search edges buffor, and break the loop. */
						SearchEdges.RemoveAt(searchEdgeIdx);
						break;
					}
				}
			}
		}

		BoundaryEdges.Append(SearchEdges);
	}

	if (!GroupEdges(BoundaryEdges, HolesEdges))
		return;

	/* At this point boundary edges should be created properly. But chaotically. Which means the edges migh not belong to the same hole one after another. We should now detect holes as separate objects. */

	/* Iterate through all holes found in mesh. */
	for (int32 holeIdx = 0; holeIdx < HolesEdges.Num(); holeIdx++)
	{
		/* Store hole edges. */
		TArray<FUtilEdge3D> HoleEdges = HolesEdges[holeIdx];

		/* Store random three vertexes of the hole, that are not equal. First one and second one will always be the first one in this hole, taken from the first edge. */
		TArray<FVector> PlanePoints;
		PlanePoints.Add(HoleEdges[0].V0);
		PlanePoints.Add(HoleEdges[0].V1);

		/* Iterate through each edge. */
		for (int32 edgeIdx = 1; edgeIdx < HoleEdges.Num(); edgeIdx++)
		{
			/* Initialize another plane point as long as plane points list is less than 3 and as long as its new point. */
			if (PlanePoints.Num() < 3)
			{
				if (!PlanePoints.Contains(HoleEdges[edgeIdx].V0))
				{
					PlanePoints.Add(HoleEdges[edgeIdx].V0);
					break;
				}
			}
		}

		/* At this point plane points should be guaranteed to be three and different, so no need to check them. */
		/* Begin setting up a hole filling polygons. */
		TArray<FUtilEdge2D> Edges2D;
		FUtilPoly2DSet PolySet;
		FTransform UniversalTransform = FTransform::Identity;
		FVector LocalPlanePos = UniversalTransform.InverseTransformPosition(PlanePoints[0]);
		FVector Dir = FVector::CrossProduct(PlanePoints[1] - PlanePoints[0], PlanePoints[2] - PlanePoints[0]);
		Dir = Dir.GetSafeNormal();
		FVector LocalPlaneNormal = UniversalTransform.InverseTransformVectorNoScale(Dir) * (-1.f);
		FPlane Plane = FPlane(LocalPlanePos, LocalPlaneNormal);
		FGeomTools::ProjectEdges(Edges2D, PolySet.PolyToWorld, HoleEdges, Plane);
		FGeomTools::Buid2DPolysFromEdges(PolySet.Polys, Edges2D, HoleCapsVertexColors);
		for (int32 PolyIdx = 0; PolyIdx < PolySet.Polys.Num(); PolyIdx++)
		{
			FGeomTools::GeneratePlanarTilingPolyUVs(PolySet.Polys[PolyIdx], 64.f);
			const int32 vertBase = Geometry.VertexBuffer.Num();
			Transform2DPolygonTo3DPolygon(PolySet.Polys[PolyIdx], PolySet.PolyToWorld, Geometry.VertexBuffer, Geometry.LocalBounds);
			TriangulatePolygon(Geometry.IndexBuffer, Geometry.VertexBuffer, vertBase, LocalPlaneNormal);
		}
	}
}

bool FProceduralGeometry::GroupEdges(const TArray<FProceduralGeometryPolygonEdge> Edges, TArray<TArray<FUtilEdge3D>>& EdgesGroups)
{
	/* Provided edges number must not be 0. */
	if (Edges.Num() < 1)
		return false;

	/* Store edges indexes that should be skipped. */
	TArray<int32> SkipEdges;

	/* Iterate through all provided edges. */
	for (int32 edgeIdx = 0; edgeIdx < Edges.Num(); edgeIdx++)
	{
		/* Skip this edge if contained within skip edges. */
		if (SkipEdges.Contains(edgeIdx))
			continue;

		/* Add this edge to the hierarchy. */
		TArray<FUtilEdge3D> Hierarchy;

		/* Find all edges along this edge's path. */
		FindWholeEdgeHierarchy(Edges, edgeIdx, SkipEdges, Hierarchy);

		/* Add found hierarchy to the edges groups. */
		EdgesGroups.Add(Hierarchy);
	}

	return EdgesGroups.Num() > 0;
}

void FProceduralGeometry::Transform2DPolygonTo3DPolygon(const FUtilPoly2D & InPoly, const FMatrix & InMatrix, TArray<FProceduralGeometryVertex>& OutVerts, FBox & OutBox)
{
	FVector PolyNormal = -InMatrix.GetUnitAxis(EAxis::Z);
	FProceduralGeometryTangent PolyTangent(InMatrix.GetUnitAxis(EAxis::X), false);

	for (int32 VertexIndex = 0; VertexIndex < InPoly.Verts.Num(); VertexIndex++)
	{
		const FUtilVertex2D& InVertex = InPoly.Verts[VertexIndex];

		FProceduralGeometryVertex NewVert;

		NewVert.Position = InMatrix.TransformPosition(FVector(InVertex.Pos.X, InVertex.Pos.Y, 0.f));
		NewVert.Normal = PolyNormal;
		NewVert.Tangent = PolyTangent;
		NewVert.Color = InVertex.Color;
		NewVert.UV0 = InVertex.UV;

		OutVerts.Add(NewVert);

		// Update bounding box
		OutBox += NewVert.Position;
	}
}

bool FProceduralGeometry::TriangulatePolygon(TArray<uint32>& OutTris, const TArray<FProceduralGeometryVertex>& PolyVerts, int32 VertBase, const FVector & PolyNormal)
{
	// Can't work if not enough verts for 1 triangle
	int32 NumVerts = PolyVerts.Num() - VertBase;
	if (NumVerts < 3)
	{
		OutTris.Add(0);
		OutTris.Add(2);
		OutTris.Add(1);

		// Return true because poly is already a tri
		return true;
	}

	// Remember initial size of OutTris, in case we need to give up and return to this size
	const int32 TriBase = OutTris.Num();

	// Init array of vert indices, in order. We'll modify this
	TArray<int32> VertIndices;
	VertIndices.AddUninitialized(NumVerts);
	for (int VertIndex = 0; VertIndex < NumVerts; VertIndex++)
	{
		VertIndices[VertIndex] = VertBase + VertIndex;
	}

	// Keep iterating while there are still vertices
	while (VertIndices.Num() >= 3)
	{
		// Look for an 'ear' triangle
		bool bFoundEar = false;
		for (int32 EarVertexIndex = 0; EarVertexIndex < VertIndices.Num(); EarVertexIndex++)
		{
			// Triangle is 'this' vert plus the one before and after it
			const int32 AIndex = (EarVertexIndex == 0) ? VertIndices.Num() - 1 : EarVertexIndex - 1;
			const int32 BIndex = EarVertexIndex;
			const int32 CIndex = (EarVertexIndex + 1) % VertIndices.Num();

			const FProceduralGeometryVertex& AVert = PolyVerts[VertIndices[AIndex]];
			const FProceduralGeometryVertex& BVert = PolyVerts[VertIndices[BIndex]];
			const FProceduralGeometryVertex& CVert = PolyVerts[VertIndices[CIndex]];

			// Check that this vertex is convex (cross product must be positive)
			const FVector ABEdge = BVert.Position - AVert.Position;
			const FVector ACEdge = CVert.Position - AVert.Position;
			const float TriangleDeterminant = (ABEdge ^ ACEdge) | PolyNormal;
			if (TriangleDeterminant > 0.f)
			{
				continue;
			}

			bool bFoundVertInside = false;
			// Look through all verts before this in array to see if any are inside triangle
			for (int32 VertexIndex = 0; VertexIndex < VertIndices.Num(); VertexIndex++)
			{
				const FProceduralGeometryVertex& TestVert = PolyVerts[VertIndices[VertexIndex]];

				if (VertexIndex != AIndex &&
					VertexIndex != BIndex &&
					VertexIndex != CIndex &&
					FGeomTools::PointInTriangle(AVert.Position, BVert.Position, CVert.Position, TestVert.Position))
				{
					bFoundVertInside = true;
					break;
				}
			}

			// Triangle with no verts inside - its an 'ear'! 
			if (!bFoundVertInside)
			{
				OutTris.Add(VertIndices[AIndex]);
				OutTris.Add(VertIndices[CIndex]);
				OutTris.Add(VertIndices[BIndex]);

				// And remove vertex from polygon
				VertIndices.RemoveAt(EarVertexIndex);

				bFoundEar = true;
				break;
			}
		}

		// If we couldn't find an 'ear' it indicates something is bad with this polygon - discard triangles and return.
		if (!bFoundEar)
		{
			OutTris.SetNum(TriBase, true);
			return false;
		}
	}

	return true;
}

void FProceduralGeometry::FindWholeEdgeHierarchy(const TArray<FProceduralGeometryPolygonEdge> Edges, const int32 SearchEdgeIndex, TArray<int32>& EdgesAssigned, TArray<FUtilEdge3D>& Hierarchy)
{
	/* Store iterated edge. */
	FProceduralGeometryPolygonEdge Edge = Edges[SearchEdgeIndex];
	EdgesAssigned.Add(SearchEdgeIndex);
	Hierarchy.Add(Edge.ToUtilEdge3D());

	/* Iterate through all edges. */
	for (int32 foundEdgeIdx = 0; foundEdgeIdx < Edges.Num(); foundEdgeIdx++)
	{
		/* Don't look up itself. */
		if (foundEdgeIdx == SearchEdgeIndex)
			continue;

		/* Skip already assigned edges. */
		if (EdgesAssigned.Contains(foundEdgeIdx))
			continue;

		/* Store found edge. */
		FProceduralGeometryPolygonEdge FoundEdge = Edges[foundEdgeIdx];

		/* Check if edges share vertices. */
		if (Edge.Shares(FoundEdge))
		{
			/* Call this method recursively for found edge. */
			FindWholeEdgeHierarchy(Edges, foundEdgeIdx, EdgesAssigned, Hierarchy);
		}
	}
}

bool FProceduralGeometry::CreateProceduralGeometryFromData(const TArray<FVector>& VertexPositions, const TArray<uint32>& Indices, const TArray<FVector>& VertexNormals, const TArray<FVector2D>& VertexUVs, const TArray<FProceduralGeometryTangent>& VertexTangents, UMaterialInterface * GeometryMaterial, const bool Visible, FProceduralGeometry & OutGeometry)
{
	if (VertexPositions.Num() != VertexNormals.Num() || VertexNormals.Num() != VertexUVs.Num() || VertexUVs.Num() != VertexTangents.Num())
		return false;

	/* Create Vertex array of the procedural vertexes. */
	TArray<FProceduralGeometryVertex> Vertexes;
	FBox GeomBox;
	GeomBox.Init();

	for (int32 i = 0; i < VertexPositions.Num(); i++)
	{
		FProceduralGeometryVertex NewVertex;
		NewVertex.Position = VertexPositions[i];
		NewVertex.Normal = VertexNormals[i];
		NewVertex.UV0 = VertexUVs[i];
		NewVertex.Tangent = VertexTangents[i];
		Vertexes.Add(NewVertex);
		GeomBox += NewVertex.Position;
	}

	const bool Success = CreateProceduralGeometry(Vertexes, Indices, GeomBox, GeometryMaterial, Visible, OutGeometry);

	return Success;
}

bool FProceduralGeometry::IsInside(const FVector TestPoint)
{
	const int TriNum = IndexBuffer.Num() / 3;

	for (int i = 0; i < TriNum; i++)
	{
		FVector V1 = VertexBuffer[IndexBuffer[i * 3]].Position;
		FVector V2 = VertexBuffer[IndexBuffer[i * 3 + 1]].Position;
		FVector V3 = VertexBuffer[IndexBuffer[i * 3 + 2]].Position;
		FPlane Plane = FPlane(V1, V2, V3);
		float PointCenterDistance = Plane.PlaneDot(TestPoint);
		if (PointCenterDistance < 0.f)
			return false;
		//UE_LOG(LogTemp, Warning, TEXT("Point test: %f"), PointCenterDistance);
	}

	UE_LOG(LogTemp, Warning, TEXT("Point BELONGS!"));
	return true;
}

FProceduralGeometrySkeleton FProceduralGeometry::DivideGeometrySkeleton(const int Division)
{
	FProceduralGeometrySkeleton OutSkeleton = Skeleton;
	for (int div = 0; div < Division; div++)
	{
		FProceduralGeometrySkeleton NewSkeleton;
		/* Iterate through each skeleton edge in this geometry. */
		for (const FEdgeIndex & Edge : OutSkeleton.EdgeBuffer)
		{
			FProceduralGeometrySkeletonVertex V0 = OutSkeleton.VertexBuffer[Edge.V0];
			FProceduralGeometrySkeletonVertex V1 = OutSkeleton.VertexBuffer[Edge.V1];

			FProceduralGeometrySkeletonVertex V3 = FProceduralGeometrySkeletonVertex::InterpolateVertex(V0, V1, 0.5f);

			FEdgeIndex EdgeFirst, EdgeSecond;
			//
			int32 idx = NewSkeleton.FindVertex(V0);
			if (idx == INDEX_NONE)
				EdgeFirst.V0 = NewSkeleton.VertexBuffer.Add(V0);
			else
				EdgeFirst.V0 = idx;
			//
			EdgeSecond.V0 = EdgeFirst.V1 = NewSkeleton.VertexBuffer.Add(V3);
			//
			idx = NewSkeleton.FindVertex(V1);
			if (idx == INDEX_NONE)
				EdgeSecond.V1 = NewSkeleton.VertexBuffer.Add(V1);
			else
				EdgeSecond.V1 = idx;
			//
			NewSkeleton.EdgeBuffer.Add(EdgeFirst);
			NewSkeleton.EdgeBuffer.Add(EdgeSecond);
		}
		//
		OutSkeleton = NewSkeleton;
	}

	return OutSkeleton;
}

TArray<FKSphylElem>& FProceduralGeometry::GetCollisionCapsules()
{
	return CollisionCapsules;
}

TArray<FKBoxElem>& FProceduralGeometry::GetCollisionBoxes()
{
	return CollisionBoxes;
}

TArray<FKSphereElem>& FProceduralGeometry::GetCollisionSpheres()
{
	return CollisionSpheres;
}

void FProceduralGeometry::GeneratePhysics(int32 Accuracy, const EGeometryCollisionGenerationType CollisionGenerationType)
{
	/* Accuracy must be beyond 0. */
	Accuracy = Accuracy < 1 ? 1 : Accuracy;

	/* Grab skeleton. */
	FProceduralGeometrySkeleton UseSkeleton = Skeleton;

	/* Divide skeleton if needed. */
	if (Accuracy > 1)
		UseSkeleton = DivideGeometrySkeleton(Accuracy - 1);

	/* Reset all collisions. */
	CollisionCapsules.Empty();
	CollisionBoxes.Empty();
	CollisionSpheres.Empty();

	/* Iterate through each skeleton edge. */
	for (int32 edgeIdx = 0; edgeIdx < UseSkeleton.GetNumEdges(); edgeIdx++)
	{
		/* Get physics data for this edge. */
		float Radius, HalfLength;
		FRotator Rotation;
		FVector Center;
		UseSkeleton.GetEdgeDataForPhysics(edgeIdx, Radius, HalfLength, Center, Rotation);
		
		/* Filter various types of coliision generation. */

		if (CollisionGenerationType == EGeometryCollisionGenerationType::GCGT_Capsule)
		{
			/* Build collision geometry for edge. */
			FKSphylElem Capsule = FKSphylElem(Radius * 15.f, HalfLength);

			/* Translate capsule to edge center. */
			Capsule.Center = Center;

			/* Rotate capsule to match edge start and end. */
			Capsule.Rotation = Rotation;

			/* Add capsule to capsules array. */
			CollisionCapsules.Add(Capsule);
		}
		else if (CollisionGenerationType == EGeometryCollisionGenerationType::GCGT_Box)
		{
			FKBoxElem Box = FKBoxElem(Radius * 23.f, Radius * 23.f, HalfLength * 2.f);
			Box.Center = Center;
			Box.Rotation = Rotation;
			CollisionBoxes.Add(Box);
		}
		else if (CollisionGenerationType == EGeometryCollisionGenerationType::GCGT_Sphere)
		{
			FKSphereElem Sphere = FKSphereElem(HalfLength / 2.f);
			Sphere.Center = Center;
			CollisionSpheres.Add(Sphere);
		}
	}
}

FProceduralGeometryID FProceduralGeometry::GetID()
{
	return ID;
}

FProceduralGeometryID FProceduralGeometry::GetID() const
{
	return ID;
}

void FProceduralGeometry::SetGeometryID(const int32 Id)
{
	if (ID.IsIDValid() || Id < 0)
		return;

	ID = FProceduralGeometryID(Id);
}

void FProceduralGeometry::AddChildGeometryID(const FProceduralGeometryID ChildID)
{
	if (!ChildID.IsIDValid())
		return;

	ChildrenIDs.Add(ChildID);
}

const TArray<FProceduralGeometryID>& FProceduralGeometry::GetChildrenGeometriesIDs() const
{
	return ChildrenIDs;
}

TArray<FVector> FProceduralGeometry::GetVertexPositions()
{
	TArray<FVector> Array;

	for (const FProceduralGeometryVertex & Vertex : VertexBuffer)
	{
		Array.Add(Vertex.Position);
	}

	return Array;
}

TArray<FVector> FProceduralGeometry::GetVertexNormals()
{
	TArray<FVector> Array;

	for (const FProceduralGeometryVertex & Vertex : VertexBuffer)
	{
		Array.Add(Vertex.Normal);
	}

	return Array;
}

TArray<FVector2D> FProceduralGeometry::GetUVs()
{
	TArray<FVector2D> Array;

	for (const FProceduralGeometryVertex & Vertex : VertexBuffer)
	{
		Array.Add(Vertex.UV0);
	}

	return Array;
}

TArray<FProceduralGeometryTangent> FProceduralGeometry::GetVertexTangents()
{
	TArray<FProceduralGeometryTangent> Array;

	for (const FProceduralGeometryVertex & Vertex : VertexBuffer)
	{
		Array.Add(Vertex.Tangent);
	}

	return Array;
}

TArray<FColor> FProceduralGeometry::GetVertexColors()
{
	TArray<FColor> Array;

	for (const FProceduralGeometryVertex & Vertex : VertexBuffer)
	{
		Array.Add(Vertex.Color);
	}

	return Array;
}

FVector FProceduralGeometry::GetPivotLocation() const
{
	if (!Skeleton.VertexBuffer.IsValidIndex(Pivot))
		return FVector(0.f);

	return Skeleton.VertexBuffer[Pivot].Position;
}

bool FProceduralGeometry::IsChild(const FProceduralGeometryID & Id) const
{
	return ChildrenIDs.Contains(Id);
}


bool FProceduralGeometry::IsGeometryValid()
{
	return VertexBuffer.Num() > 0 && IndexBuffer.Num() > 0;
}

FVector FProceduralGeometry::GetPivotPosition() const 
{
	return Skeleton.VertexBuffer.IsValidIndex(Pivot) ? Skeleton.VertexBuffer[Pivot].Position : FVector(0.f);
}
