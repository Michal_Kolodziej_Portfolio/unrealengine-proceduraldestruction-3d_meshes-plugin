#include "Multithreading.h"

FManagerOperationWorker::FManagerOperationWorker(UProceduralGeometryManager *InManager, UManagerOperation *InOperation)
	: pManager(InManager)
	, pOperation(InOperation)
{
	/* If manager and operation are valid, proceed with new thread. */
	if (pManager == nullptr || pOperation == nullptr)
		return;
	
	/* Initialize thread for this worker. */
	/* Thread name will include operation number to be unique. */
	const FString ThreadName = "FManagerOperationWorker_" + FString::FromInt(InOperation->m_operationId);
	pThread = FRunnableThread::Create(this, *ThreadName, 0, TPri_BelowNormal);
}

FManagerOperationWorker::~FManagerOperationWorker()
{
	/* Perform cleanup for a thread. */
	delete pThread;
	pThread = nullptr;
}

bool FManagerOperationWorker::Init()
{
	/* Init will return false if manager or operation is not valid. */
	return pManager != nullptr && pOperation != nullptr;
}

uint32 FManagerOperationWorker::Run()
{
	/* Actual processing algorightms for the operation. */
	UProceduralGeometryManager::OnProcessOperation(pOperation);
	m_bFinished = true;
	return 0;
}

bool FManagerOperationWorker::IsFinished()
{
	return m_bFinished;
}