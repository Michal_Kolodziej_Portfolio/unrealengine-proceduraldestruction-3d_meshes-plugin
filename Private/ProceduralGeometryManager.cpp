#include "ProceduralGeometryManager.h"
#include "DrawDebugHelpers.h"
#include "Multithreading.h"

#include "Editor/EditorEngine.h"
UManagerOperation * UManagerOperation::CreateOperation
(
	int32 operationId,
	UProceduralGeometryComponent * pComponent, 
	EOperationType type, 
	EOperationMode mode,
	EOperationQueueMode queueMode,
	FVector firstLocation,
	FVector secondLocation,
	FVector normalFirst,
	FVector normalSecond,
	bool useSkeletonNormal,
	bool useSecondLocation,
	bool generateOtherHalf,
	FColor capVertexColor,
	FProceduralGeometryID affectedGeometryID
)
{
	UManagerOperation *pOperation = NewObject<UManagerOperation>();
	pOperation->m_operationId = operationId;
	pOperation->m_pComponent = pComponent;
	pOperation->m_type = type;
	pOperation->m_mode = mode;
	pOperation->m_queueMode = queueMode;
	pOperation->m_firstLocation = firstLocation;
	pOperation->m_secondLocation = secondLocation;
	pOperation->m_normalFirst = normalFirst;
	pOperation->m_normalSecond = normalSecond;
	pOperation->m_useSkeletonNormal = useSkeletonNormal;
	pOperation->m_useSecondLocation = useSecondLocation;
	pOperation->m_generateOtherHalf = generateOtherHalf;
	pOperation->m_capVertexColor = capVertexColor;
	pOperation->m_affectedGeometryID = affectedGeometryID;
	//
	return pOperation;
}

UManagerOperation::~UManagerOperation()
{
	/* If operation thread is valid, clean it up in destructor. */
	if (m_pThread != nullptr)
	{
		delete m_pThread;
		m_pThread = nullptr;
	}
}

void UProceduralGeometryManager::BeginPlay(AGameModeBase *GameModeBase)
{
	/* Iterate through asset actors and initialize them. */
	//for (UStaticProceduralGeometry *pAssetGeometry : ProceduralGeometryAssets)
	//{
	//	FActorSpawnParameters Params;
	//	Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	//	/* Spawn actor. */
	//	AActor *pAssetActor = GetWorld()->SpawnActor<AActor>(AActor::StaticClass(), FVector(0.f, 0.f, 0.f), FRotator(0.f, 0.f, 0.f), Params);

	//	/* Iterate through asset geometry geometries. */
	//	for (const FProceduralGeometry Geometry : pAssetGeometry->Geometries)
	//	{
	//		UProceduralGeometryComponent *pComponent = NewObject<UProceduralGeometryComponent>(pAssetActor);
	//		pComponent->RegisterComponent();
	//		if (!IsValid(pAssetActor->GetRootComponent()))
	//			pAssetActor->SetRootComponent(pComponent);
	//		else
	//			pComponent->AttachToComponent(pAssetActor->GetRootComponent(), FAttachmentTransformRules::KeepWorldTransform);

	//		pComponent->AddGeometry(Geometry);
	//		pComponent->Update();
	//		pComponent->GeneratePhysics();
	//	}

	//	pAssetActor->SetActorLocation(FVector(0.f, 0.f, -10000.f));
	//}
}

void UProceduralGeometryManager::UpdateManager()
{
	/* If current operation is being processed in other thread, make sure to check here whether it is finished, and if so, call operation finished callback. Also return from here, so no regular process operation is called. */
	if (m_pCurrentOperation->m_pThread != nullptr)
	{
		if(m_pCurrentOperation->m_pThread->IsFinished())
			OnCurrentOperationFinished();

		return;
	}

	ProcessOperation(m_pCurrentOperation);
}

int32 UProceduralGeometryManager::BeginOperation
(
	UProceduralGeometryComponent * Component,
	const EOperationType Type,
	const EOperationMode Mode, 
	const EOperationQueueMode QueueMode,
	const FVector LocationFirst, 
	const FVector LocationSecond,
	const FVector NormalFirst,
	const FVector NormalSecond, 
	const bool UseSkeletonNormal, 
	const bool UseSecondLocation, 
	const bool GenerateSecondHalf,
	const FLinearColor CapVertexColor,
	const FProceduralGeometryID AffectedGeometryID
)
{
	/* Component must be valid! */
	if (!IsValid(Component))
		return INDEX_NONE;

	/* Before creating new operation, we need to reserve new unique id for it. */
	const int32 NewOperationId = m_highestOperationId++;

	/* Create new operation. */
	UManagerOperation *pNewOperation = UManagerOperation::CreateOperation	
	(
		NewOperationId,
		Component,
		Type, 
		Mode,
		QueueMode,
		LocationFirst,
		LocationSecond,
		NormalFirst,
		NormalSecond,
		UseSkeletonNormal,
		UseSecondLocation,
		GenerateSecondHalf,
		CapVertexColor.ToFColor(false),
		AffectedGeometryID
	);

	/* If new operation queue mode is instant, then it should be added as current operation only. */
	if (QueueMode == EOperationQueueMode::OQM_Instant)
	{
		m_operationsQueue.Insert(pNewOperation, 0);
		m_pCurrentOperation = nullptr;
	}

	/* Otherwise if operation queue mode is queued, then add it to queue. */
	else if(QueueMode == EOperationQueueMode::OQM_Queued)
		m_operationsQueue.Add(pNewOperation);

	/* If there is no current operation being processed, make the first one in the operations queue begin. */
	if (!IsValid(m_pCurrentOperation))
		m_pCurrentOperation = m_operationsQueue[0];

	return NewOperationId;
}

float UProceduralGeometryManager::VectorDistance(const FVector & V0, const FVector & V1)
{
	return FVector::Dist(V0, V1);
}

void UProceduralGeometryManager::ProcessOperation(UManagerOperation *pOperation)
{
	/* Check what operation type should be done on it. */
	/* If geometry slice should be done. */
	/* If operation mode is multithreaded, then process everything in side the other thread. */
	if (pOperation->m_mode == EOperationMode::OM_MultiThreaded)
	{
		pOperation->m_pThread = new FManagerOperationWorker(this, pOperation);
	}
	else
	{
		OnProcessOperation(pOperation);
		OnCurrentOperationFinished();
	}
}

void UProceduralGeometryManager::OnProcessOperation(UManagerOperation *pOperation)
{
	/* Decide what to actually do with this operation. */
	if (pOperation->m_type == EOperationType::OT_SliceGeometry)
		SliceGeometry(pOperation);
}

void UProceduralGeometryManager::SliceGeometry(UManagerOperation *pOperation)
{
	/* Initialize slice planes and their relative values. */
	const FTransform ComponentToWorld = pOperation->m_pComponent->GetComponentTransform();
	//
	pOperation->m_slicePlanePosFirst = ComponentToWorld.InverseTransformPosition(pOperation->m_firstLocation) + pOperation->m_pComponent->GetRootPivot();

	/* If user requires, recalculate normal to slice along. */
	if (pOperation->m_useSkeletonNormal)
		pOperation->m_slicePlaneNormalFirst = pOperation->m_normalFirst = pOperation->m_pComponent->GetClosestSkeletonVertex(pOperation->m_slicePlanePosFirst, true, false).Normal.GetSafeNormal();
	else
		pOperation->m_slicePlaneNormalFirst = ComponentToWorld.InverseTransformVectorNoScale(pOperation->m_normalFirst).GetSafeNormal();

	pOperation->m_slicePlaneFirst = FPlane(pOperation->m_slicePlanePosFirst, pOperation->m_slicePlaneNormalFirst);

	/* Initialize second location plane in the same way, but with inverted normal. Only if second location is to be used. */
	if (pOperation->m_useSecondLocation)
	{
		pOperation->m_slicePlanePosSecond = ComponentToWorld.InverseTransformPosition(pOperation->m_secondLocation) + pOperation->m_pComponent->GetRootPivot();

		if (pOperation->m_useSkeletonNormal)
			pOperation->m_slicePlaneNormalSecond = pOperation->m_normalSecond = -pOperation->m_pComponent->GetClosestSkeletonVertex(pOperation->m_slicePlanePosSecond, true, false).Normal.GetSafeNormal();
		else
			pOperation->m_slicePlaneNormalSecond = ComponentToWorld.InverseTransformVectorNoScale(-pOperation->m_normalSecond).GetSafeNormal();

		pOperation->m_slicePlaneSecond = FPlane(pOperation->m_slicePlanePosSecond, pOperation->m_slicePlaneNormalSecond);
	}

	/* Store geometries indexes for faster iteration. */
	pOperation->m_pComponent->GetByID(pOperation->m_affectedGeometryID, pOperation->m_currentGeometry);

	/* Perform box plane compare based on single or double slice. */
	if (pOperation->m_useSecondLocation)
	{
		/* Initialize geometry that will be forwarded to output in the end. */
		FProceduralGeometry::CreateProceduralGeometry(pOperation->m_currentGeometry.Material, pOperation->m_currentGeometry.GeometryVisible, pOperation->m_currentGeometry.GetChildrenGeometriesIDs(), pOperation->m_currentGeometry.GetID(), pOperation->m_firstOutGeometry);

		/* Matches source geometry vertex list. Stores indexes of the sliced equivalent vertices or -1 if no equivalent was assigned. */
		TArray<int32> BaseToSlicedV;

		/* Distances between vertexes and slice planes for each slice plane. All of the base geometry vertexes are saved into it, so that its indexes match input geometry vertex buffer. */
		TArray<float> VertDistance[2];

		/* Matches input geometry vertex buffer. If vertex is not sliced off, it has -1, if it is, it has 0 if first plane sliced it, or 1 if second one. */
		TArray<int32> VertSlicedBy;

		/* Iterate through each vertex. */
		for (int32 BaseVertIdx = 0; BaseVertIdx < pOperation->m_currentGeometry.VertexBuffer.Num(); BaseVertIdx++)
		{
			/* Store iterated vertex. */
			FProceduralGeometryVertex BaseVert = pOperation->m_currentGeometry.VertexBuffer[BaseVertIdx];

			/* Measure distance between each plane and this vertex. */
			const float DistanceFirst = pOperation->m_slicePlaneFirst.PlaneDot(BaseVert.Position);
			const float DistanceSecond = pOperation->m_slicePlaneSecond.PlaneDot(BaseVert.Position);

			/* Add distances to the distances array. */
			VertDistance[0].Add(DistanceFirst);
			VertDistance[1].Add(DistanceSecond);

			BaseToSlicedV.Add(-1);

			/* If vertex survived slice by the first and second plane, add it to output geometry buffer. */
			if (DistanceFirst > 0.f && DistanceSecond > 0.f)
			{
				/* Add surviving vertex to output geometry and save index in this buffer. */
				const int32 SlicedVertIdx = pOperation->m_firstOutGeometry.VertexBuffer.Add(BaseVert);
				/* Update local bounds of the output geometry. */
				pOperation->m_firstOutGeometry.LocalBounds += BaseVert.Position;
				/* Map this vertex inside input geometry buffer to output geometry buffer. */
				//BaseToSlicedV.Emplace(BaseVertIdx, SlicedVertIdx);
				BaseToSlicedV[BaseToSlicedV.Num() - 1] = SlicedVertIdx;
				/* Define which plane sliced this vertex. In this case none. */
				VertSlicedBy.Add(-1);
			}
			/* If vertex is sliced by some plane, check and save by which one. */
			else
			{
				if (DistanceFirst <= 0.f)
					VertSlicedBy.Add(0);

				else if (DistanceSecond <= 0.f)
					VertSlicedBy.Add(1);
			}
		}

		/* Iterate through each triangle. */
		for (int32 BaseIdx = 0; BaseIdx < pOperation->m_currentGeometry.IndexBuffer.Num(); BaseIdx += 3)
		{
			/* Store all vertices of this triangle. */
			int32 BaseV[3];
			/* Store all vertices that survived slicing inside output geometry. */
			int32 SlicedV[3] = { -1, -1, -1 };

			/* Iterate through each vertex in this triangle. */
			for (int32 i = 0; i < 3; i++)
			{
				/* Retrieve indexes for base triangle. */
				BaseV[i] = pOperation->m_currentGeometry.IndexBuffer[BaseIdx + i];
				/* Retrieve vertexes that survived inside output geometry. */
				SlicedV[i] = BaseToSlicedV[BaseV[i]];
			}

			/* If all vertices of this triangle survived slicing, add entire triangle to output section. */
			if (SlicedV[0] != -1 && SlicedV[1] != -1 && SlicedV[2] != -1)
			{
				pOperation->m_firstOutGeometry.IndexBuffer.Add(SlicedV[0]);
				pOperation->m_firstOutGeometry.IndexBuffer.Add(SlicedV[1]);
				pOperation->m_firstOutGeometry.IndexBuffer.Add(SlicedV[2]);
			}

			/* If all vertices have been sliced off, do nothing. */
			else if (SlicedV[0] == -1 && SlicedV[1] == -1 && SlicedV[2] == -1)
			{
				/* ... */
			}

			/* If there are some vertices that actually survived, and some that didn't, we need to interpolate between these vertexes and find intersections. */
			else
			{
				/* Final vertices storage. Maximum 5 if both planes share one slice edge. */
				int32 FinalVerts[5];
				int32 NumFinalVerts = 0;

				/* Store clipped edges for building cap later. */
				FUtilEdge3D NewClipEdge[2];
				int32 ClippedEdges[2] = { 0, 0 };

				/* Iterate through each edge in this triangle. */
				for (int32 EdgeIdx = 0; EdgeIdx < 3; EdgeIdx++)
				{
					/* Store indexes of V0 and V1 in this edge. */
					int32 V0 = EdgeIdx;
					int32 V1 = (EdgeIdx + 1) % 3;

					int32 PlaneIdx = -1;

					/* If V0 is surviving one, add it straight to final verts. */
					if (SlicedV[V0] != -1)
					{
						check(NumFinalVerts < 5);
						FinalVerts[NumFinalVerts++] = SlicedV[V0];

						/* Also see if V1 has been sliced by some plane. */
						PlaneIdx = VertSlicedBy[BaseV[V1]];
					}
					/* If V0 hasn't survived, but V1 has, check which plane sliced V0. */
					else if (SlicedV[V1] != -1)
						PlaneIdx = VertSlicedBy[BaseV[V0]];

					/* If there is some plane that sliced any vertex. */
					if (PlaneIdx != -1)
					{
						/* We need to find intersection vertex here. */
						const float Alpha = -VertDistance[PlaneIdx][BaseV[V0]] / (VertDistance[PlaneIdx][BaseV[V1]] - VertDistance[PlaneIdx][BaseV[V0]]);
						const FProceduralGeometryVertex InterpVertex = FProceduralGeometryVertex::InterpolateVertex(pOperation->m_currentGeometry.VertexBuffer[BaseV[V0]], pOperation->m_currentGeometry.VertexBuffer[BaseV[V1]], FMath::Clamp(Alpha, 0.f, 1.f));

						/* Add interpolated vertex to output geometry and store its index. */
						const int32 InterpVertIdx = pOperation->m_firstOutGeometry.VertexBuffer.Add(InterpVertex);

						/* Make sure final vertices are no more than 5. */
						check(NumFinalVerts < 5);
						/* Add interpolated vertex index to final vertices indexes. */
						FinalVerts[NumFinalVerts++] = InterpVertIdx;

						/* There might be no more clipped edges than 2 for single edge. */
						check(ClippedEdges[PlaneIdx] < 2);

						/* Create new clip edge based on interpolated vertex. */
						if (ClippedEdges[PlaneIdx] == 0)
							NewClipEdge[PlaneIdx].V0 = InterpVertex.Position;
						else
							NewClipEdge[PlaneIdx].V1 = InterpVertex.Position;

						ClippedEdges[PlaneIdx]++;
					}
				}

				/* Iterate through final verts and build triangles between them. */
				for (int32 VertIdx = 2; VertIdx < NumFinalVerts; VertIdx++)
				{
					pOperation->m_firstOutGeometry.IndexBuffer.Add(FinalVerts[0]);
					pOperation->m_firstOutGeometry.IndexBuffer.Add(FinalVerts[VertIdx - 1]);
					pOperation->m_firstOutGeometry.IndexBuffer.Add(FinalVerts[VertIdx]);
				}

				/* Throw assert when clipped edges are only 1 for each slice plane. */
				check(ClippedEdges[0] != 1);
				check(ClippedEdges[1] != 1);

				/* Store final clipped edges in operation. */
				if (ClippedEdges[0] == 2)
					pOperation->m_clipEdgesFirst.Add(NewClipEdge[0]);

				if (ClippedEdges[1] == 2)
					pOperation->m_clipEdgesSecond.Add(NewClipEdge[1]);
			}
		}

		/* Slice skeleton for current geometry and save it into output first geometry. */
		SliceSkeleton(pOperation->m_currentGeometry, pOperation->m_slicePlaneFirst, pOperation->m_slicePlaneSecond, pOperation->m_firstOutGeometry.Skeleton, pOperation->m_firstOutGeometry.Pivot, false);

		GenerateCap(pOperation->m_firstOutGeometry, pOperation->m_clipEdgesFirst, pOperation->m_clipEdgesSecond, pOperation->m_slicePlaneFirst, pOperation->m_slicePlaneSecond, pOperation->m_slicePlaneNormalFirst, pOperation->m_slicePlaneNormalSecond, pOperation->m_capVertexColor);
	}
	else
	{
		/* Initialize both geometries without geometry polygonal data. */
		FProceduralGeometry::CreateProceduralGeometry(pOperation->m_currentGeometry.Material, pOperation->m_currentGeometry.GeometryVisible, pOperation->m_currentGeometry.GetChildrenGeometriesIDs(), pOperation->m_currentGeometry.GetID(), pOperation->m_firstOutGeometry);
		FProceduralGeometry::CreateProceduralGeometry(pOperation->m_currentGeometry.Material, pOperation->m_currentGeometry.GeometryVisible, pOperation->m_currentGeometry.GetChildrenGeometriesIDs(), pOperation->m_currentGeometry.GetID(), pOperation->m_secondOutGeometry);

		/* These match source geometry vertex buffer and store indexes of the sliced vertices or -1 if non existing. */
		TArray<int32> FirstHalfBaseToSlicedVertIndex;
		TArray<int32> SecondHalfBaseToSlicedVertIndex;

		/* Number of vertexes in this processed geometry. */
		const int32 NumBaseVerts = pOperation->m_currentGeometry.VertexBuffer.Num();

		/* Distance of each base vertex from slice plane. */
		TArray<float> VertDistance;

		/* Initialize whole array by the empty object in number of vertices. */
		VertDistance.AddUninitialized(NumBaseVerts);

		/* Actually process geometry. */
		/* Iterate through all vertexes in the input geometry. */
		for (int32 BaseVertIdx = 0; BaseVertIdx < pOperation->m_currentGeometry.VertexBuffer.Num(); BaseVertIdx++)
		{
			/* Store currently iterated vertex. */
			FProceduralGeometryVertex BaseVert = pOperation->m_currentGeometry.VertexBuffer[BaseVertIdx];

			/* Calculate distance from plane. */
			VertDistance[BaseVertIdx] = pOperation->m_slicePlaneFirst.PlaneDot(BaseVert.Position);

			FirstHalfBaseToSlicedVertIndex.Add(-1);
			SecondHalfBaseToSlicedVertIndex.Add(-1);

			/* See if vertex is being kept in this geometry. */
			if (VertDistance[BaseVertIdx] > 0.f)
			{
				/* Copy to sliced vertex buffer. */
				int32 SlicedVertIndex = pOperation->m_firstOutGeometry.VertexBuffer.Add(BaseVert);
				/* Update geometry bounds. */
				pOperation->m_firstOutGeometry.LocalBounds += BaseVert.Position;
				/* Add to map. */
				FirstHalfBaseToSlicedVertIndex[FirstHalfBaseToSlicedVertIndex.Num() - 1] = SlicedVertIndex;
			}
			/* Otherwise, if vertex is not being kept in this geometry, but second half is required, keep it in there. */
			else if (pOperation->m_generateOtherHalf)
			{
				/* Copy to sliced vertex buffer. */
				int32 SlicedVertIndex = pOperation->m_secondOutGeometry.VertexBuffer.Add(BaseVert);
				/* Update geometry bounds. */
				pOperation->m_secondOutGeometry.LocalBounds += BaseVert.Position;
				/* Add to map. */
				SecondHalfBaseToSlicedVertIndex[SecondHalfBaseToSlicedVertIndex.Num() - 1] = SlicedVertIndex;
			}
		}
		/* Iterate through indices of the input geometry and resolve them. */
		for (int32 BaseIndex = 0; BaseIndex < pOperation->m_currentGeometry.IndexBuffer.Num(); BaseIndex += 3)
		{
			/* Base triangles in original geometry. */
			int32 BaseV[3];
			/* Pointers to triangles in new vertex buffer. */
			int32 FirstHalfSlicedV[3] = { -1, -1, -1 };
			/* Pointers to triangles in new vertex buffer for second half if required. */
			int32 SecondHalfSlicedV[3] = { -1, -1, -1 };

			/* Iterate through all three vertexes in this triangle. */
			for (int32 i = 0; i < 3; i++)
			{
				/* Get triangle vertex index. */
				BaseV[i] = pOperation->m_currentGeometry.IndexBuffer[BaseIndex + i];
				/* Map base vertex to sliced one. */
				FirstHalfSlicedV[i] = FirstHalfBaseToSlicedVertIndex[BaseV[i]];
				/* THe same thing for second half if required. */
				if (pOperation->m_generateOtherHalf)
				{
					SecondHalfSlicedV[i] = SecondHalfBaseToSlicedVertIndex[BaseV[i]];
					/* Each base vert must exist in either of halves. */
					check((FirstHalfSlicedV[i] != -1) != (SecondHalfSlicedV[i] != -1));
				}
			}

			/* If all vertexes survived plane cull, keep the triangle. */
			if (FirstHalfSlicedV[0] != -1 && FirstHalfSlicedV[1] != -1 && FirstHalfSlicedV[2] != -1)
			{
				pOperation->m_firstOutGeometry.IndexBuffer.Add(FirstHalfSlicedV[0]);
				pOperation->m_firstOutGeometry.IndexBuffer.Add(FirstHalfSlicedV[1]);
				pOperation->m_firstOutGeometry.IndexBuffer.Add(FirstHalfSlicedV[2]);
			}

			/* Otherwise, if all vertexes were removed by the plane cull, add them to the second half if required. */
			else if (FirstHalfSlicedV[0] == -1 && FirstHalfSlicedV[1] == -1 && FirstHalfSlicedV[2] == -1)
			{
				if (pOperation->m_generateOtherHalf)
				{
					pOperation->m_secondOutGeometry.IndexBuffer.Add(SecondHalfSlicedV[0]);
					pOperation->m_secondOutGeometry.IndexBuffer.Add(SecondHalfSlicedV[1]);
					pOperation->m_secondOutGeometry.IndexBuffer.Add(SecondHalfSlicedV[2]);
				}
			}

			/* Finally if partially culled, clip to create 1 or 2 new triangles. */
			else
			{
				int32 FirstHalfFinalVerts[4];
				int32 FirstHalfNumFinalVerts = 0;

				int32 SecondHalfFinalVerts[4];
				int32 SecondHalfNumFinalVerts = 0;

				FUtilEdge3D NewClipEdge;
				int32 ClippedEdges = 0;

				float PlaneDist[3];
				PlaneDist[0] = VertDistance[BaseV[0]];
				PlaneDist[1] = VertDistance[BaseV[1]];
				PlaneDist[2] = VertDistance[BaseV[2]];

				/* Iterate through edges. */
				for (int32 EdgeIndex = 0; EdgeIndex < 3; EdgeIndex++)
				{
					int32 ThisVert = EdgeIndex;

					/* If start vertex is inside add it to the first half. */
					if (FirstHalfSlicedV[ThisVert] != -1)
					{
						check(FirstHalfNumFinalVerts < 4);
						FirstHalfFinalVerts[FirstHalfNumFinalVerts++] = FirstHalfSlicedV[ThisVert];
					}

					/* Otherwise, add it to second half. */
					else if (pOperation->m_generateOtherHalf)
					{
						check(SecondHalfNumFinalVerts < 4);
						SecondHalfFinalVerts[SecondHalfNumFinalVerts++] = SecondHalfSlicedV[ThisVert];
					}

					/* If start and next vertex are on opposite sides, add intersection. */
					int32 NextVert = (EdgeIndex + 1) % 3;

					if ((FirstHalfSlicedV[EdgeIndex] == -1) != (FirstHalfSlicedV[NextVert] == -1))
					{
						/* Find distance along edge that plane is. */
						float Alpha = -PlaneDist[ThisVert] / (PlaneDist[NextVert] - PlaneDist[ThisVert]);
						/* Interpolate vertex params to that point. */
						FProceduralGeometryVertex InterpolatedVertex = FProceduralGeometryVertex::InterpolateVertex(pOperation->m_currentGeometry.VertexBuffer[BaseV[ThisVert]], pOperation->m_currentGeometry.VertexBuffer[BaseV[NextVert]], FMath::Clamp(Alpha, 0.f, 1.f));
						/* Add to vertex buffer. */
						int32 InterpolatedVertexIndex = pOperation->m_firstOutGeometry.VertexBuffer.Add(InterpolatedVertex);
						/* Update bounds. */
						pOperation->m_firstOutGeometry.LocalBounds += InterpolatedVertex.Position;
						/* Save vertex index for this polygon. */
						check(FirstHalfNumFinalVerts < 4);
						FirstHalfFinalVerts[FirstHalfNumFinalVerts++] = InterpolatedVertexIndex;

						/* If required second half, add polygon for it as well. */
						if (pOperation->m_generateOtherHalf)
						{
							int32 SecondHalfInterpolatedVertexIndex = pOperation->m_secondOutGeometry.VertexBuffer.Add(InterpolatedVertex);
							pOperation->m_secondOutGeometry.LocalBounds += InterpolatedVertex.Position;
							check(SecondHalfNumFinalVerts < 4);
							SecondHalfFinalVerts[SecondHalfNumFinalVerts++] = SecondHalfInterpolatedVertexIndex;
						}

						/* When we make a new edge on the surface of the clip plane, save it off. */
						check(ClippedEdges < 2);
						if (ClippedEdges == 0)
							NewClipEdge.V0 = InterpolatedVertex.Position;
						else
							NewClipEdge.V1 = InterpolatedVertex.Position;

						ClippedEdges++;
					}
				}

				/* Triangulate clipped polygon. */
				for (int32 VertexIndex = 2; VertexIndex < FirstHalfNumFinalVerts; VertexIndex++)
				{
					pOperation->m_firstOutGeometry.IndexBuffer.Add(FirstHalfFinalVerts[0]);
					pOperation->m_firstOutGeometry.IndexBuffer.Add(FirstHalfFinalVerts[VertexIndex - 1]);
					pOperation->m_firstOutGeometry.IndexBuffer.Add(FirstHalfFinalVerts[VertexIndex]);
				}

				/* Triangulate also second half if required. */
				if (pOperation->m_generateOtherHalf)
				{
					for (int32 VertexIndex = 2; VertexIndex < SecondHalfNumFinalVerts; VertexIndex++)
					{
						pOperation->m_secondOutGeometry.IndexBuffer.Add(SecondHalfFinalVerts[0]);
						pOperation->m_secondOutGeometry.IndexBuffer.Add(SecondHalfFinalVerts[VertexIndex - 1]);
						pOperation->m_secondOutGeometry.IndexBuffer.Add(SecondHalfFinalVerts[VertexIndex]);
					}
				}

				/* Should never clip just one edge of triangle. */
				check(ClippedEdges != 1);

				/* If we create a new edge, save it off here as well. */
				if (ClippedEdges == 2)
					pOperation->m_clipEdgesFirst.Add(NewClipEdge);
			}
		}

		/* At this point, slice skeleton too. */
		SliceSkeleton(pOperation->m_currentGeometry, pOperation->m_slicePlaneFirst, pOperation->m_firstOutGeometry.Skeleton, pOperation->m_secondOutGeometry.Skeleton, pOperation->m_secondOutGeometry.Pivot, pOperation->m_firstOutGeometry.Pivot);

		GenerateCap(pOperation->m_firstOutGeometry, pOperation->m_secondOutGeometry, pOperation->m_generateOtherHalf, pOperation->m_clipEdgesFirst, pOperation->m_slicePlaneFirst, pOperation->m_slicePlaneNormalFirst, pOperation->m_capVertexColor);
	}
}

void UProceduralGeometryManager::OnCurrentOperationFinished()
{
	OnOperationFinishedEvent
	(
		m_pCurrentOperation->m_operationId,
		m_pCurrentOperation->m_pComponent,
		m_pCurrentOperation->m_firstOutGeometry, 
		m_pCurrentOperation->m_secondOutGeometry,
		m_pCurrentOperation->m_type,
		m_pCurrentOperation->m_mode,
		m_pCurrentOperation->m_firstLocation,
		m_pCurrentOperation->m_secondLocation,
		m_pCurrentOperation->m_useSecondLocation,
		m_pCurrentOperation->m_affectedGeometryID
	);

	/* Delete current operation from queue. */
	m_operationsQueue.Remove(m_pCurrentOperation);
	
	m_pCurrentOperation->ConditionalBeginDestroy();
	m_pCurrentOperation = nullptr;

	/* If there is more than zero operations, start the next one. */
	if (m_operationsQueue.Num() > 0)
		m_pCurrentOperation = m_operationsQueue[0];
}

EBoxPlaneCompare UProceduralGeometryManager::ProcessBoxPlaneCompare(const FBox & InBox, const FPlane & InPlane)
{
	FVector BoxCenter, BoxExtents;
	InBox.GetCenterAndExtents(BoxCenter, BoxExtents);

	// Find distance of box center from plane
	float BoxCenterDist = InPlane.PlaneDot(BoxCenter);

	// See size of box in plane normal direction
	float BoxSize = FVector::BoxPushOut(InPlane, BoxExtents);

	if (BoxCenterDist > BoxSize)
	{
		return EBoxPlaneCompare::BPC_FullyInside;
	}
	else if (BoxCenterDist < -BoxSize)
	{
		return EBoxPlaneCompare::BPC_FullyOutside;
	}
	else
	{
		return EBoxPlaneCompare::BPC_PartiallyInside;
	}
}

void UProceduralGeometryManager::Transform2DPolygonTo3DPolygon(const FUtilPoly2D & InPoly, const FMatrix & InMatrix, TArray<FProceduralGeometryVertex> & OutVerts, FBox & OutBox)
{
	FVector PolyNormal = -InMatrix.GetUnitAxis(EAxis::Z);
	FProceduralGeometryTangent PolyTangent(InMatrix.GetUnitAxis(EAxis::X), false);

	for (int32 VertexIndex = 0; VertexIndex < InPoly.Verts.Num(); VertexIndex++)
	{
		const FUtilVertex2D& InVertex = InPoly.Verts[VertexIndex];

		FProceduralGeometryVertex NewVert;

		NewVert.Position = InMatrix.TransformPosition(FVector(InVertex.Pos.X, InVertex.Pos.Y, 0.f));
		NewVert.Normal = PolyNormal;
		NewVert.Tangent = PolyTangent;
		NewVert.Color = InVertex.Color;
		NewVert.UV0 = InVertex.UV;
		NewVert.Test = true;

		OutVerts.Add(NewVert);

		// Update bounding box
		OutBox += NewVert.Position;
	}
}

bool UProceduralGeometryManager::TriangulatePolygon(TArray<uint32> & OutTris, const TArray<FProceduralGeometryVertex> & PolyVerts, int32 VertBase, const FVector & PolyNormal, const bool ReverseCap)
{
	// Can't work if not enough verts for 1 triangle
	int32 NumVerts = PolyVerts.Num() - VertBase;
	if (NumVerts < 3)
	{
		OutTris.Add(0);
		OutTris.Add(2);
		OutTris.Add(1);

		// Return true because poly is already a tri
		return true;
	}

	// Remember initial size of OutTris, in case we need to give up and return to this size
	const int32 TriBase = OutTris.Num();

	// Init array of vert indices, in order. We'll modify this
	TArray<int32> VertIndices;
	VertIndices.AddUninitialized(NumVerts);
	for (int VertIndex = 0; VertIndex < NumVerts; VertIndex++)
	{
		VertIndices[VertIndex] = VertBase + VertIndex;
	}

	// Keep iterating while there are still vertices
	while (VertIndices.Num() >= 3)
	{
		// Look for an 'ear' triangle
		bool bFoundEar = false;
		for (int32 EarVertexIndex = 0; EarVertexIndex < VertIndices.Num(); EarVertexIndex++)
		{
			// Triangle is 'this' vert plus the one before and after it
			const int32 AIndex = (EarVertexIndex == 0) ? VertIndices.Num() - 1 : EarVertexIndex - 1;
			const int32 BIndex = EarVertexIndex;
			const int32 CIndex = (EarVertexIndex + 1) % VertIndices.Num();

			const FProceduralGeometryVertex& AVert = PolyVerts[VertIndices[AIndex]];
			const FProceduralGeometryVertex& BVert = PolyVerts[VertIndices[BIndex]];
			const FProceduralGeometryVertex& CVert = PolyVerts[VertIndices[CIndex]];

			// Check that this vertex is convex (cross product must be positive)
			const FVector ABEdge = BVert.Position - AVert.Position;
			const FVector ACEdge = CVert.Position - AVert.Position;
			const float TriangleDeterminant = (ABEdge ^ ACEdge) | PolyNormal;

			//UE_LOG(LogTemp, Warning, TEXT("ABEdge = %f | %f | %f"), ABEdge.X, ABEdge.Y, ABEdge.Z);
			//UE_LOG(LogTemp, Warning, TEXT("ACEdge = %f | %f | %f"), ACEdge.X, ACEdge.Y, ACEdge.Z);
			//UE_LOG(LogTemp, Warning, TEXT("AVert = %f | %f | %f"), AVert.Position.X, AVert.Position.Y, AVert.Position.Z);
			//UE_LOG(LogTemp, Warning, TEXT("BVert = %f | %f | %f"), BVert.Position.X, BVert.Position.Y, BVert.Position.Z);
			//UE_LOG(LogTemp, Warning, TEXT("CVert = %f | %f | %f"), CVert.Position.X, CVert.Position.Y, CVert.Position.Z);
			//UE_LOG(LogTemp, Warning, TEXT("AIndex = %d | BIndex = %d | CIndex = %d | TriangleDeterminant: %f"), AIndex, BIndex, CIndex, TriangleDeterminant);

			if (TriangleDeterminant > 0.f)
			{
				continue;
			}

			bool bFoundVertInside = false;
			// Look through all verts before this in array to see if any are inside triangle
			for (int32 VertexIndex = 0; VertexIndex < VertIndices.Num(); VertexIndex++)
			{
				const FProceduralGeometryVertex& TestVert = PolyVerts[VertIndices[VertexIndex]];

				if (VertexIndex != AIndex &&
					VertexIndex != BIndex &&
					VertexIndex != CIndex &&
					FGeomTools::PointInTriangle(AVert.Position, BVert.Position, CVert.Position, TestVert.Position))
				{
					bFoundVertInside = true;
					break;
				}
			}

			// Triangle with no verts inside - its an 'ear'! 
			if (!bFoundVertInside)
			{
				if (!ReverseCap)
				{
					OutTris.Add(VertIndices[AIndex]);
					OutTris.Add(VertIndices[CIndex]);
					OutTris.Add(VertIndices[BIndex]);
				}
				else
				{
					OutTris.Add(VertIndices[AIndex]);
					OutTris.Add(VertIndices[BIndex]);
					OutTris.Add(VertIndices[CIndex]);
				}

				// And remove vertex from polygon
				VertIndices.RemoveAt(EarVertexIndex);

				bFoundEar = true;
				break;
			}
		}

		// If we couldn't find an 'ear' it indicates something is bad with this polygon - discard triangles and return.
		if (!bFoundEar)
		{
			OutTris.SetNum(TriBase, true);
			return false;
		}
	}

	return true;
}

void UProceduralGeometryManager::GenerateCap(FProceduralGeometry & InGeometry, FProceduralGeometry & InSecondHalf, const bool UseSecondHalf, TArray<FUtilEdge3D> ClipEdges, FPlane SlicePlane, FVector SlicePlaneNormal, FColor CapsVertexColors)
{
	/* Make sure clip edges are not empty. */
	if (ClipEdges.Num() < 1)
		return;

	/* Project 3D edges onto slice plane to form 2D edges. */
	TArray<FUtilEdge2D> Edges2D;
	FUtilPoly2DSet PolySet;
	FGeomTools::ProjectEdges(Edges2D, PolySet.PolyToWorld, ClipEdges, SlicePlane);

	/* Find 2D closed polygins from this edge soup. */
	FGeomTools::Buid2DPolysFromEdges(PolySet.Polys, Edges2D, CapsVertexColors);

	/* Remember start point for vert and index buffer before adding an cap geometry. */
	const int32 FirstHalfBaseIdx = InGeometry.VertexBuffer.Num();
	const int32 FirstHalfIndexBase = InGeometry.IndexBuffer.Num();

	/* Triangulate each polygon. */
	for (int32 PolyIndex = 0; PolyIndex < PolySet.Polys.Num(); PolyIndex++)
	{
		/* Generate UVs for the 2D polygon. */
		FGeomTools::GeneratePlanarTilingPolyUVs(PolySet.Polys[PolyIndex], 64.f);

		/* Transform from 2D poly verts to 3D. */
		Transform2DPolygonTo3DPolygon(PolySet.Polys[PolyIndex], PolySet.PolyToWorld, InGeometry.VertexBuffer, InGeometry.LocalBounds);

		/* Triangulate this polygon. */
		TriangulatePolygon(InGeometry.IndexBuffer, InGeometry.VertexBuffer, FirstHalfBaseIdx, SlicePlaneNormal, false);
	}

	if (UseSecondHalf)
	{
		const int32 SecondHalfBaseIdx = InSecondHalf.VertexBuffer.Num();
		
		for (int32 VertIdx = FirstHalfBaseIdx; VertIdx < InGeometry.VertexBuffer.Num(); VertIdx++)
		{
			FProceduralGeometryVertex NewVert = InGeometry.VertexBuffer[VertIdx];
			NewVert.Normal *= -1.f;
			NewVert.Tangent.TangentX *= -1.f;

			InSecondHalf.VertexBuffer.Add(NewVert);
			InSecondHalf.LocalBounds += NewVert.Position;
		}

		int32 VertOffset = SecondHalfBaseIdx - FirstHalfBaseIdx;

		for (int32 IndexIdx = FirstHalfIndexBase; IndexIdx < InGeometry.IndexBuffer.Num(); IndexIdx += 3)
		{
			InSecondHalf.IndexBuffer.Add(InGeometry.IndexBuffer[IndexIdx + 0] + VertOffset);
			InSecondHalf.IndexBuffer.Add(InGeometry.IndexBuffer[IndexIdx + 2] + VertOffset);
			InSecondHalf.IndexBuffer.Add(InGeometry.IndexBuffer[IndexIdx + 1] + VertOffset);
		}
	}
}

void UProceduralGeometryManager::SliceSkeleton(const FProceduralGeometry & InGeometry, const FPlane & SlicePlane, FProceduralGeometrySkeleton & OutputASkeleton, FProceduralGeometrySkeleton & OutputBSkeleton, int32 & OutSkeletonPivotB, int32 & OutSkeletonPivotA)
{
	/* Zero out output skeleton in case if user passed something in. */
	OutputASkeleton.Clear();
	OutputBSkeleton.Clear();

	/* Store passed geometry skeleton. */
	const FProceduralGeometrySkeleton & InSkeleton = InGeometry.Skeleton;

	/* Check geometry skeleton validity. */
	if (!InSkeleton.IsSkeletonValid())
		return;

	/* Store A side generated vertexes, which means those that will get created by slice. */
	TArray<int32> A_GeneratedVerts;

	/* Iterate through all skeleton vertexes to see which side they lay at. */
	TMap<int32, int32> BaseToSlicedV_A;
	TMap<int32, int32> BaseToSlicedV_B;

	/* Store sliced vertex index to plane distance, because it might be used multiple times, so we don't have to recalculate it. */
	TArray<float> VertDistance_A;
	TArray<float> VertDistance_B;

	for (int32 vertIdx = 0; vertIdx < InSkeleton.VertexBuffer.Num(); vertIdx++)
	{
		/* Store vertex iterated. */
		const FProceduralGeometrySkeletonVertex & Vert = InSkeleton.VertexBuffer[vertIdx];

		/* Perform plane dot compare. */
		const float Dist = SlicePlane.PlaneDot(Vert.Position);

		/* If vertex polarization is positive, add it to A side. */
		if (Dist > 0.f)
		{
			/* Map base vertex index to this vertex in new skeleton. */
			BaseToSlicedV_A.Emplace(vertIdx, OutputASkeleton.VertexBuffer.Add(Vert));
			VertDistance_A.Add(Dist);
		}
		/* Otherwise, if polarization is 0 or less, add it to B side. */
		else
		{
			BaseToSlicedV_B.Emplace(vertIdx, OutputBSkeleton.VertexBuffer.Add(Vert));
			VertDistance_B.Add(Dist);
		}
	}

	/* Now that we have our vertexes separated to both skeletons, we need to actually slice right edges. */
	/* So find sliced edges. */
	/* Iterate through all edges in the skeleton. */
	for (const FEdgeIndex & Edge : InSkeleton.EdgeBuffer)
	{
		/* We need to find out which side this edge's V0 and V1 belong to. Both ends of the edge belong to A by default, but will be modified if found otherwise. */
		bool V0_To_B = false;
		bool V1_To_B = false;

		/* If the edge contains each end in different buffer, it must be sliced, so add it to sliced edges buffer. */
		/* Firstly, check where V0 exists. */
		int32 *V0_SlicedIdx = BaseToSlicedV_A.Find(Edge.V0);

		/* If V0 was not found in A side, get its index inside B side. */
		if (V0_SlicedIdx == nullptr)
		{
			V0_SlicedIdx = BaseToSlicedV_B.Find(Edge.V0);
			V0_To_B = true;
		}

		/* Perform the same checks for V1. */
		int32 *V1_SlicedIdx = BaseToSlicedV_A.Find(Edge.V1);

		if (V1_SlicedIdx == nullptr)
		{
			V1_SlicedIdx = BaseToSlicedV_B.Find(Edge.V1);
			V1_To_B = true;
		}

		/* If V0 and V1 belong to A side, add it there. */
		if (!V0_To_B && !V1_To_B)
		{
			FEdgeIndex NewEdge;
			NewEdge.V0 = *V0_SlicedIdx;
			NewEdge.V1 = *V1_SlicedIdx;
			OutputASkeleton.EdgeBuffer.Add(NewEdge);
		}
		/* Otherwise, if V0 and V1 belong to B, add it there. */
		else if (V0_To_B && V1_To_B)
		{
			FEdgeIndex NewEdge;
			NewEdge.V0 = *V0_SlicedIdx;
			NewEdge.V1 = *V1_SlicedIdx;
			OutputBSkeleton.EdgeBuffer.Add(NewEdge);
		}
		/* Otherwise, we need to find interpolation vertex between these vertexes. */
		else
		{
			/* Store V0 and V1 vertex. */
			const FProceduralGeometrySkeletonVertex & V0 = V0_To_B ? OutputBSkeleton.VertexBuffer[*V0_SlicedIdx] : OutputASkeleton.VertexBuffer[*V0_SlicedIdx];
			const FProceduralGeometrySkeletonVertex & V1 = V1_To_B ? OutputBSkeleton.VertexBuffer[*V1_SlicedIdx] : OutputASkeleton.VertexBuffer[*V1_SlicedIdx];

			/* Find alpha to interpolate between vertexes. */
			const float V0_Dist = V0_To_B ? VertDistance_B[*V0_SlicedIdx] : VertDistance_A[*V0_SlicedIdx];
			const float V1_Dist = V1_To_B ? VertDistance_B[*V1_SlicedIdx] : VertDistance_A[*V1_SlicedIdx];

			const float Alpha = -V0_Dist / (V1_Dist - V0_Dist);

			/* Store interpolated vertex. */
			const FProceduralGeometrySkeletonVertex InterpVert = FProceduralGeometrySkeletonVertex::InterpolateVertex(V0, V1, FMath::Clamp(Alpha, 0.f, 1.f));

			/* Store new edge for both sides. */
			FEdgeIndex A_Edge, B_Edge;

			/* Now if V0 belongs to A, add it as V0, and make InterpVert V1. */
			if (!V0_To_B)
			{
				A_Edge.V0 = *V0_SlicedIdx;
				A_Edge.V1 = OutputASkeleton.VertexBuffer.Add(InterpVert);
				/* V0 should be closer to the potential pivot, so flip the edge if needed. */
				A_Edge.FlipEdge();
				A_GeneratedVerts.Add(A_Edge.V0);
			}
			/* Otherwise, if V0 belongs to B, add it as V0, and make InterpVert V1. */
			else
			{
				B_Edge.V0 = *V0_SlicedIdx;
				B_Edge.V1 = OutputBSkeleton.VertexBuffer.Add(InterpVert);
			}

			/* Perform the same conditions for V1. */
			if (!V1_To_B)
			{
				A_Edge.V1 = *V1_SlicedIdx;
				A_Edge.V0 = OutputASkeleton.VertexBuffer.Add(InterpVert);
				A_GeneratedVerts.Add(A_Edge.V0);
			}
			else
			{
				B_Edge.V1 = *V1_SlicedIdx;
				B_Edge.V0 = OutputBSkeleton.VertexBuffer.Add(InterpVert);
			}

			/* Now add both edges to their respective sides. */
			OutputASkeleton.EdgeBuffer.Add(A_Edge);
			OutputBSkeleton.EdgeBuffer.Add(B_Edge);
		}
	}

	/* A side is particular example here. Its sliced edges are saved into buffer. */
	/* We need to avoid situations, where there are multiple edges sliced, and there is no mutual middle point to them, that connects them. */
	/* Now iterate through all sliced A side vertexes to find middle vertex between them. */
	/* New pivot for A side will be preferably mid slice index at position 0, or the interpolated one, either way, store it here, so it can be accessed later when setting up pivot. */
	OutSkeletonPivotA = A_GeneratedVerts[0];

	/* It all only happens if generated vertexes count is more than 1. */
	if (A_GeneratedVerts.Num() > 1)
	{
		FProceduralGeometrySkeletonVertex MidSliceVert = OutputASkeleton.VertexBuffer[A_GeneratedVerts[0]];

		for (int32 i = 1; i < A_GeneratedVerts.Num(); i++)
		{
			/* Store iterated vertex. */
			const FProceduralGeometrySkeletonVertex & Vert = OutputASkeleton.VertexBuffer[A_GeneratedVerts[i]];

			/* Blend in the middle between this vertex and the middle slice one. */
			const FProceduralGeometrySkeletonVertex InterpVert = FProceduralGeometrySkeletonVertex::InterpolateVertex(MidSliceVert, Vert, 0.5f);

			/* Now this interpolated vertex becomes last one. */
			MidSliceVert = InterpVert;
		}

		/* Add newly created mid slice vert into vertex buffer and store its index. */
		OutSkeletonPivotA = OutputASkeleton.VertexBuffer.Add(MidSliceVert);

		/* Build edges between mid sliced one, and all generated ones, where the generated ones are always V1. */
		for (const int32 & i : A_GeneratedVerts)
		{
			/* Build edge and add it to A side buffer. */
			FEdgeIndex Edge;
			Edge.V1 = i;
			Edge.V0 = OutSkeletonPivotA;
			OutputASkeleton.EdgeBuffer.Add(Edge);
		}
	}

	/* Now that skeleton is properly sliced, create pivots for both sides. */
	/* B side skeleton will derive pivot from input skeleton, but probably with changed index. */
	int32 * BPivot = BaseToSlicedV_B.Find(InGeometry.Pivot);
	if (BPivot != nullptr)
		OutSkeletonPivotB = *BPivot;
}

void UProceduralGeometryManager::GenerateCap(FProceduralGeometry & InGeometry, TArray<FUtilEdge3D> ClipEdgesFirst, TArray<FUtilEdge3D> ClipEdgesSecond, FPlane SlicePlaneFirst, FPlane SlicePlaneSecond, FVector SlicePlaneNormalFirst, FVector SlicePlaneNormalSecond, FColor CapsVertexColors)
{
	/* Make sure clip edges for any plane are not empty. */
	if (ClipEdgesFirst.Num() < 1 || ClipEdgesSecond.Num() < 1)
		return;

	/* Project 3D edges onto slice planes to form 2D edges. */
	TArray<FUtilEdge2D> Edges2D[2];
	FUtilPoly2DSet PolySet[2];
	/* Build polygons and edges. */
	FGeomTools::ProjectEdges(Edges2D[0], PolySet[0].PolyToWorld, ClipEdgesFirst, SlicePlaneFirst);
	FGeomTools::Buid2DPolysFromEdges(PolySet[0].Polys, Edges2D[0], CapsVertexColors);
	FGeomTools::ProjectEdges(Edges2D[1], PolySet[1].PolyToWorld, ClipEdgesSecond, SlicePlaneSecond);
	FGeomTools::Buid2DPolysFromEdges(PolySet[1].Polys, Edges2D[1], CapsVertexColors);

	/* Remember start point for vertex for input geometry. */
	int32 PolyVertBase = InGeometry.VertexBuffer.Num();

	/* Triangulate each polygon for first plane slice. */
	for (int32 PolyIndex = 0; PolyIndex < PolySet[0].Polys.Num(); PolyIndex++)
	{
		/* Generate EVs for the 2D polygon. */
		FGeomTools::GeneratePlanarTilingPolyUVs(PolySet[0].Polys[PolyIndex], 64.f);

		/* Transform from 2D poly verts to 3D. */
		Transform2DPolygonTo3DPolygon(PolySet[0].Polys[PolyIndex], PolySet[0].PolyToWorld, InGeometry.VertexBuffer, InGeometry.LocalBounds);

		/* Triangulate this polygon. */
		TriangulatePolygon(InGeometry.IndexBuffer, InGeometry.VertexBuffer, PolyVertBase, SlicePlaneNormalFirst, false);
	}

	/* Perform exactly the same thing for B slice. */
	PolyVertBase = InGeometry.VertexBuffer.Num();

	/* Triangulate each polygon for first plane slice. */
	for (int32 PolyIndex = 0; PolyIndex < PolySet[1].Polys.Num(); PolyIndex++)
	{
		FGeomTools::GeneratePlanarTilingPolyUVs(PolySet[1].Polys[PolyIndex], 64.f);
		Transform2DPolygonTo3DPolygon(PolySet[1].Polys[PolyIndex], PolySet[1].PolyToWorld, InGeometry.VertexBuffer, InGeometry.LocalBounds);
		TriangulatePolygon(InGeometry.IndexBuffer, InGeometry.VertexBuffer, PolyVertBase, SlicePlaneNormalSecond, false);
	}
}

void UProceduralGeometryManager::SliceSkeleton(const FProceduralGeometry & InGeometry, const FPlane & FirstPlane, const FPlane & SecondPlane, FProceduralGeometrySkeleton & OutputSkeleton, int32 & OutSkeletonPivot, const bool SecondPlaneMakesPivot)
{
	/* Zero out output skeleton in case if user passed something in. */
	OutputSkeleton.Clear();
	
	/* Store passed geometry skeleton. */
	const FProceduralGeometrySkeleton & InSkeleton = InGeometry.Skeleton;

	/* Check geometry skeleton validity. */
	if (!InSkeleton.IsSkeletonValid())
		return;

	/* Store A and B slice plane generated vertexes, so those interpolated ones that were created on runtime. */
	TArray<int32> GeneratedVerts;

	/* Iterate through all skeleton vertexes to see which side they lay at. */
	TMap<int32, int32> BaseToSlicedV;

	/* Store BASE vertex index to plane distance, because it might be used multiple times, so we don't have to recalculate it. */
	TArray<float> VertDistance_A;
	TArray<float> VertDistance_B;

	for (int32 vertIdx = 0; vertIdx < InSkeleton.VertexBuffer.Num(); vertIdx++)
	{
		/* Store iterated vertex. */
		const FProceduralGeometrySkeletonVertex & Vert = InSkeleton.VertexBuffer[vertIdx];

		/* Check distance between each plane and this vertex. */
		const float Dist_A = FirstPlane.PlaneDot(Vert.Position);
		const float Dist_B = SecondPlane.PlaneDot(Vert.Position);

		/* In order for the vertex to be inside the slice, its polarization needs to be positive to both planes. */
		if (Dist_A > 0.f && Dist_B > 0.f)
		{
			/* Add this vertex to new skeleton vertex buffer and store its distance from the plane. */
			BaseToSlicedV.Emplace(vertIdx, OutputSkeleton.VertexBuffer.Add(Vert));
		}

		/* Store vertex to plane distance. */
		VertDistance_A.Add(Dist_A);
		VertDistance_B.Add(Dist_B);
	}

	/* Now that we have our vertexes saved, we need to slice right edges. */
	/* Iterate through all edges in skeleton. */
	for (const FEdgeIndex & Edge : InSkeleton.EdgeBuffer)
	{
		/* Store V0 and V1 vertexes. */
		const FProceduralGeometrySkeletonVertex & V0 = InSkeleton.VertexBuffer[Edge.V0];
		const FProceduralGeometrySkeletonVertex & V1 = InSkeleton.VertexBuffer[Edge.V1];

		/* Store edge V0 and V1 within new skeleton buffer. */
		const int32 *V0_SlicedIdx = BaseToSlicedV.Find(Edge.V0);
		const int32 *V1_SlicedIdx = BaseToSlicedV.Find(Edge.V1);

		/* Store new edge for output skeleton. */
		FEdgeIndex NewEdge;

		/* If both V0 and V1 have not been found to be inside the slice, this edge disappears. */
		/* If V0 is inside the slice. */
		if (V0_SlicedIdx != nullptr)
		{
			/* NewEdge V0 will equal V0_SlicedIdx. */
			NewEdge.V0 = *V0_SlicedIdx;

			/* If V1 requires slicing. */
			if (V1_SlicedIdx == nullptr)
			{
				/* Generate interpolation alphas for both slice planes scenarios. */
				float Alpha = 0.f;

				if (VertDistance_A[Edge.V1] <= 0.f)
					Alpha = -VertDistance_A[Edge.V0] / (VertDistance_A[Edge.V1] - VertDistance_A[Edge.V0]);
				else if (VertDistance_B[Edge.V1] <= 0.f)
					Alpha = -VertDistance_B[Edge.V0] / (VertDistance_B[Edge.V1] - VertDistance_B[Edge.V0]);

				/* Create interpolated vertex between V0 and V1 by the selected alpha. */
				const FProceduralGeometrySkeletonVertex InterpVert = FProceduralGeometrySkeletonVertex::InterpolateVertex(V0, V1, FMath::Clamp(Alpha, 0.f, 1.f));

				/* Add interp vertex to the output skeleton vertex buffer and make it V1 for the new edge. */
				NewEdge.V1 = OutputSkeleton.VertexBuffer.Add(InterpVert);

				/* Depending on which plane should generate pivot, flip edge. */
				/* If plane A dictates pivot. */
				if (!SecondPlaneMakesPivot)
				{
					/* Then V0 must be closer to plane A, so if V1 is sliced by plane A, flip edge. */
					if (VertDistance_A[Edge.V1] <= 0.f)
						NewEdge.FlipEdge();
				}
				/* If plane B dictates pivot. */
				else
				{
					/* Then V0 must be closer to plane B, so if V1 is sliced by plane B, flip edge. */
					if (VertDistance_B[Edge.V1] <= 0.f)
						NewEdge.FlipEdge();
				}

				/* Lastly, add this interpolated vertex to generated vertexes array. */
				GeneratedVerts.Add(NewEdge.V0);
			}
			/* Otherwise if V1 is contained too, make it V1 for the new edge. */
			else
				NewEdge.V1 = *V1_SlicedIdx;
		}
		/* If lastly V0 is not inside the slice, but V1 is. */
		else if (V1_SlicedIdx != nullptr)
		{
			/* New edge's V1 will become V1_SlicedIdx. */
			NewEdge.V1 = *V1_SlicedIdx;

			/* Perform interpolation for V0 for both planes, depending on which one sliced it. */
			float Alpha = 0.f;
			
			if (VertDistance_A[Edge.V0] <= 0.f)
				Alpha = -VertDistance_A[Edge.V1] / (VertDistance_A[Edge.V0] - VertDistance_A[Edge.V1]);
			else if (VertDistance_B[Edge.V0] <= 0.f)
				Alpha = -VertDistance_B[Edge.V1] / (VertDistance_B[Edge.V0] - VertDistance_B[Edge.V1]);

			/* Create interpolated vertex between V1 and V0 by the selected alpha. */
			const FProceduralGeometrySkeletonVertex InterpVert = FProceduralGeometrySkeletonVertex::InterpolateVertex(V1, V0, FMath::Clamp(Alpha, 0.f, 1.f));

			/* Add interp vertex to the output skeleton vertex buffer and make it V0 for the new edge. */
			NewEdge.V0 = OutputSkeleton.VertexBuffer.Add(InterpVert);

			/* Depending on which plane should generate pivot, flip edge. */
			/* If plane A dictates pivot. */
			if (!SecondPlaneMakesPivot)
			{
				/* Then V0 must be closer to plane A, so if V0 is sliced by plane B, flip edge. */
				if (VertDistance_B[Edge.V0] <= 0.f)
					NewEdge.FlipEdge();
			}
			/* If plane B dictates pivot. */
			else
			{
				/* Then V0 must be closer to plane B, so if V0 is sliced by plane A, flip edge. */
				if (VertDistance_A[Edge.V0] <= 0.f)
					NewEdge.FlipEdge();
			}

			/* Lastly, add this interpolated vertex to generated vertexes array. */
			GeneratedVerts.Add(NewEdge.V0);
		}

		/* At last, if the new edge is valid, add it to output skeleton. */
		if (NewEdge.IsValid())
			OutputSkeleton.EdgeBuffer.Add(NewEdge);
	}

	/* Make sure generated verts are non 0. */
	if (GeneratedVerts.Num() < 1)
		return;

	/* Now that the output skeleton is ready, we can find output pivot for output skeleton. */
	/* We will blend between generated ones. */
	OutSkeletonPivot = GeneratedVerts[0];

	/* It all only happens if generated vertexes count is more than 1. */
	if (GeneratedVerts.Num() > 1)
	{
		FProceduralGeometrySkeletonVertex MidSliceVert = OutputSkeleton.VertexBuffer[GeneratedVerts[0]];

		for (int32 i = 1; i < GeneratedVerts.Num(); i++)
		{
			/* Store iterated vertex. */
			const FProceduralGeometrySkeletonVertex & Vert = OutputSkeleton.VertexBuffer[GeneratedVerts[i]];

			/* Blend in the middle between this vertex and the middle slice one. */
			const FProceduralGeometrySkeletonVertex InterpVert = FProceduralGeometrySkeletonVertex::InterpolateVertex(MidSliceVert, Vert, 0.5f);

			/* Now this interpolated vertex becomes last one. */
			MidSliceVert = InterpVert;
		}

		/* Add newly created mid slice vert into vertex buffer and store its index. */
		OutSkeletonPivot = OutputSkeleton.VertexBuffer.Add(MidSliceVert);

		/* Build edges between mid sliced one, and all generated ones, where the generated ones are always V1. */
		for (const int32 & i : GeneratedVerts)
		{
			/* Build edge and add it to output skeleton edges buffer. */
			FEdgeIndex Edge;
			Edge.V1 = i;
			Edge.V0 = OutSkeletonPivot;
			OutputSkeleton.EdgeBuffer.Add(Edge);
		}
	}
}