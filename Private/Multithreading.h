#pragma once
#include "Engine.h"
#include "ProceduralGeometryManager.h"

class FManagerOperationWorker : public FRunnable
{
public:
	FManagerOperationWorker(UProceduralGeometryManager *InManager, UManagerOperation *InOperation);
	virtual ~FManagerOperationWorker();
	virtual bool Init();
	virtual uint32 Run();
	bool IsFinished();

private:
	FRunnableThread *pThread = nullptr;
	UProceduralGeometryManager *pManager = nullptr;
	UManagerOperation *pOperation = nullptr;
	bool m_bFinished = false;
};